const handleAuthentication = require('./authentication-handler');
const http = require('http');

function start(io) {
    const chatio = io.of('/chat');
    chatio.use(handleAuthentication);

    chatio.on('connection', socket => {
        console.log('New socket connected: ' + socket.id);
        let currentSocketChatID;
        let isTeam;
        socket.on('chat_info', info => {
            console.log('New Chat Info received: ' + JSON.stringify(info));
            if (info.chat_id) {
                currentSocketChatID = info.chat_id;
                isTeam = !!info.is_team;
                socket.join(currentSocketChatID);
            } else {
                chatio.to(socket.id).emit('chat_error', 'Empty user info sent');
            }
        });
        socket.on('message', message => {
            console.log('New Message received: ' + message);
            if (currentSocketChatID) {
                const options = {
                    hostname: 'localhost',
                    port: 8080,
                    path: isTeam ? '/api/chat/team/' + currentSocketChatID : '/api/chat/private/' + currentSocketChatID,
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'X-XSRF-TOKEN': socket.request.csrfToken,
                        'Cookie': 'XSRF-TOKEN=' + socket.request.csrfToken + '; SESSION=' + socket.request.sessionId
                    }
                };
                const req = http.request(options, (res) => {
                    if (res.statusCode !== 200) {
                        if (res.statusCode === 401) {
                            chatio.to(currentSocketChatID).emit('not_authenticated', 'Not authenticated');
                            console.log('Not authenticated');
                            return;
                        }
                        console.log(res.statusCode);
                        console.log('Message not saved');
                    } else {
                        console.log('Messages saved');
                    }
                    chatio.to(currentSocketChatID).emit('chat_message', {author: 'MessageAuthor', message: message});
                });
                req.write(message);
                req.end();
            } else {
                chatio.to(socket.id).emit('chat_error', 'Socket is not identified');
            }
        });
    });

    console.log('Chat Server is started');
}

module.exports = start;
