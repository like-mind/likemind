const handleAuthentication = require('./authentication-handler');
const pg = require('pg');
const EventEmitter = require('events');
const postgresqlUrl = process.env.POSTGRESQL_URL || 'postgresql://postgres:123@localhost:5432/likemind';

function start(io) {
    const dbEventEmitter = new EventEmitter();
    const pool = new pg.Pool({
        connectionString: postgresqlUrl
    });
    pool.connect(function(err, client) {
        if (err) {
            console.log('Error establishing PostgreSQL connection: ' + err);
        }

        client.on('notification', function(msg) {
            const notification = JSON.parse(msg.payload);
            dbEventEmitter.emit(notification.receiver_id, {id: notification.id, content: notification.content});
        });

        client.query('LISTEN new_notification');
    });

    const notificationio = io.of('/notifications');
    notificationio.use(handleAuthentication);

    notificationio.on('connection', socket => {
        console.log('New socket connected: ' + socket.id);
        console.log('New user connected: ' + socket.user_id);

        dbEventEmitter.on(socket.user_id, notification => {
            notificationio.to(socket.id).emit('notification', notification);
        });
    });

    console.log('Notification Server is started');
}

module.exports = start;
