const http = require('http');
const cookie = require('cookie');

const appHost = process.env.API_HOST || 'localhost';
const appPort = process.env.API_HOST ? 80 : 8080;

function handleAuthentication(socket, callback) {
    const cookies = cookie.parse(socket.request.headers.cookie);
    const sessionId = cookies['SESSION'];
    const csrfToken = cookies['XSRF-TOKEN'];
    if (!sessionId || !csrfToken) {
        return callback(new Error('not_authenticated'));
    }

    socket.request.sessionId = sessionId;
    socket.request.csrfToken = csrfToken;

    const req = http.request({
        hostname: appHost,
        port: appPort,
        path: '/api/users/current',
        method: 'POST',
        headers: {
            'X-XSRF-TOKEN': csrfToken,
            'Cookie': 'XSRF-TOKEN=' + csrfToken + '; SESSION=' + sessionId
        }
    }, res => {
        console.log('Status: ', res.statusCode);
        if (res.statusCode !== 200) {
            if (res.statusCode === 401) {
                callback(new Error('not_authenticated'));
                return;
            }
            callback(new Error('server_error'));
        }
        console.log('User is authenticated');
        res.setEncoding('utf8');
        let data = '';
        res.on('data', (chunk) => {
            data += chunk;
        });
        res.on('end', () => {
            socket.user_id = JSON.parse(data).id;
            callback();
        });
    });
    req.end();
}

module.exports = handleAuthentication;
