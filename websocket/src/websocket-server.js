const port = process.env.PORT || 3000;
const io = require('socket.io')(port);
const startChatServer = require('./chat-server');
const startNotificationServer = require('./notification-server');

startChatServer(io);
startNotificationServer(io);