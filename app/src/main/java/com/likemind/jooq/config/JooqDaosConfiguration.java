package com.likemind.jooq.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.likemind.db.model.tables.daos.ChatMessageDao;
import com.likemind.db.model.tables.daos.NotificationDao;
import com.likemind.db.model.tables.daos.PrivateChatDao;
import com.likemind.db.model.tables.daos.ProjectDao;
import com.likemind.db.model.tables.daos.SocialConnectionDao;
import com.likemind.db.model.tables.daos.TeamDao;
import com.likemind.db.model.tables.daos.TeamRequestDao;
import com.likemind.db.model.tables.daos.UserDao;

@Configuration
public class JooqDaosConfiguration {

    @Bean
    public static TeamDao teamDao(org.jooq.Configuration configuration) {
        return new TeamDao(configuration);
    }

    @Bean
    public static ChatMessageDao chatMessageDao(org.jooq.Configuration configuration) {
        return new ChatMessageDao(configuration);
    }

    @Bean
    public static PrivateChatDao privateChatDao(org.jooq.Configuration configuration) {
        return new PrivateChatDao(configuration);
    }

    @Bean
    public static UserDao userDao(org.jooq.Configuration configuration) {
        return new UserDao(configuration);
    }

    @Bean
    public static TeamRequestDao teamRequestDao(org.jooq.Configuration configuration) {
        return new TeamRequestDao(configuration);
    }

    @Bean
    public static ProjectDao projectDao(org.jooq.Configuration configuration) {
        return new ProjectDao(configuration);
    }

    @Bean
    public static SocialConnectionDao socialConnectionDao(org.jooq.Configuration configuration) {
        return new SocialConnectionDao(configuration);
    }

    @Bean
    public static NotificationDao notificationDao(org.jooq.Configuration configuration) {
        return new NotificationDao(configuration);
    }
}
