package com.likemind.jooq.custom;

public enum NotificationType {

    PRIVATE_CHAT_MESSAGE, TEAM_REQUEST, TEAM_CHAT_MESSAGE
}
