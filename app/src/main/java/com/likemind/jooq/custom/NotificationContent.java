package com.likemind.jooq.custom;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(property = "type", use = JsonTypeInfo.Id.NAME)
@JsonSubTypes({
        @JsonSubTypes.Type(name = "privateChatMessage", value = NotificationContent.PrivateChatMessage.class),
        @JsonSubTypes.Type(name = "teamChatMessage", value = NotificationContent.TeamChatMessage.class),
        @JsonSubTypes.Type(name = "teamRequest", value = NotificationContent.TeamRequest.class)})
public class NotificationContent {

    public String message;

    public NotificationContent() {
    }

    public NotificationContent(String message) {
        this.message = message;
    }

    public static class PrivateChatMessage extends NotificationContent {
        public String chatId;

        public PrivateChatMessage() {
        }

        public PrivateChatMessage(String firstName, String lastName, String chatId) {
            super(String.format("User %s %s sent a message", firstName, lastName));
            this.chatId = chatId;
        }
    }

    public static class TeamChatMessage extends NotificationContent {
        public String teamId;

        public TeamChatMessage() {
        }

        public TeamChatMessage(String firstName, String lastName, String teamId) {
            super(String.format("User %s %s sent a message", firstName, lastName));
            this.teamId = teamId;
        }
    }

    public static class TeamRequest extends NotificationContent {
        public String projectId;

        public TeamRequest() {
        }

        public TeamRequest(String firstName, String lastName, String projectId) {
            super(String.format("User %s %s would like to join to your team", firstName, lastName));
            this.projectId = projectId;
        }
    }
}
