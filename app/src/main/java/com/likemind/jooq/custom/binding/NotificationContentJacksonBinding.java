package com.likemind.jooq.custom.binding;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.Types;
import java.util.Objects;

import org.jooq.Binding;
import org.jooq.BindingGetResultSetContext;
import org.jooq.BindingGetSQLInputContext;
import org.jooq.BindingGetStatementContext;
import org.jooq.BindingRegisterContext;
import org.jooq.BindingSQLContext;
import org.jooq.BindingSetSQLOutputContext;
import org.jooq.BindingSetStatementContext;
import org.jooq.Converter;
import org.jooq.conf.ParamType;
import org.jooq.impl.DSL;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.likemind.jooq.custom.NotificationContent;

public class NotificationContentJacksonBinding implements Binding<Object, NotificationContent> {

    private final ObjectMapper objectMapper;

    public NotificationContentJacksonBinding() {
        objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_ABSENT);
    }

    @Override
    public Converter<Object, NotificationContent> converter() {
        return new Converter<>() {
            @Override
            public NotificationContent from(Object t) {
                try {
                    return t == null ? null : objectMapper.readValue("" + t, NotificationContent.class);
                } catch (IOException e) {
                    throw new UncheckedIOException(e);
                }
            }

            @Override
            public Object to(NotificationContent u) {
                try {
                    return u == null ? null : objectMapper.writeValueAsString(u);
                } catch (JsonProcessingException e) {
                    throw new UncheckedIOException(e);
                }
            }

            @Override
            public Class<Object> fromType() {
                return Object.class;
            }

            @Override
            public Class<NotificationContent> toType() {
                return NotificationContent.class;
            }
        };
    }

    @Override
    public void sql(BindingSQLContext<NotificationContent> ctx) {
        if (ctx.render().paramType() == ParamType.INLINED) {
            ctx.render().visit(DSL.inline(ctx.convert(converter()).value())).sql("::json");
        } else {
            ctx.render().sql("?::json");
        }
    }

    @Override
    public void register(BindingRegisterContext<NotificationContent> ctx) throws SQLException {
        ctx.statement().registerOutParameter(ctx.index(), Types.VARCHAR);
    }

    @Override
    public void set(BindingSetStatementContext<NotificationContent> ctx) throws SQLException {
        ctx.statement().setString(ctx.index(), Objects.toString(ctx.convert(converter()).value(), null));
    }

    @Override
    public void set(BindingSetSQLOutputContext<NotificationContent> ctx) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }

    @Override
    public void get(BindingGetResultSetContext<NotificationContent> ctx) throws SQLException {
        ctx.convert(converter()).value(ctx.resultSet().getString(ctx.index()));
    }

    @Override
    public void get(BindingGetStatementContext<NotificationContent> ctx) throws SQLException {
        ctx.convert(converter()).value(ctx.statement().getString(ctx.index()));
    }

    @Override
    public void get(BindingGetSQLInputContext<NotificationContent> ctx) throws SQLException {
        throw new SQLFeatureNotSupportedException();
    }
}
