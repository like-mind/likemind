package com.likemind.jooq.custom;

public enum UserAvailability {

    LESS_THAN_TWENTY, FROM_TWENTY_TO_FORTY, MORE_THAN_FORTY;
}
