package com.likemind.jooq.custom;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

public enum UserRole {

    USER,
    ADMIN(USER);

    private static final String ROLE_PREFIX = "ROLE_";

    public Collection<? extends GrantedAuthority> authorities;

    UserRole(UserRole... includedRoles) {
        Set<GrantedAuthority> authorities = new HashSet<>(AuthorityUtils.createAuthorityList(ROLE_PREFIX + this.name()));
        for (UserRole includedRole : includedRoles) {
            authorities.addAll(includedRole.authorities);
        }
        this.authorities = authorities;
    }
}
