package com.likemind.social.repo;

import java.time.Instant;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.jooq.DSLContext;
import org.springframework.social.ApiException;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.MultiValueMap;

import com.google.common.collect.ImmutableList;
import com.google.common.primitives.Ints;
import com.likemind.db.model.Tables;
import com.likemind.db.model.tables.daos.SocialConnectionDao;
import com.likemind.db.model.tables.daos.UserDao;
import com.likemind.db.model.tables.pojos.SocialConnection;
import com.likemind.jooq.custom.UserRole;
import com.likemind.s3.AmazonS3Service;

public class UsersConnectionRepository implements org.springframework.social.connect.UsersConnectionRepository {

    private final SocialConnectionDao socialConnectionDao;
    private final DSLContext dslContext;
    private final AmazonS3Service s3Service;
    private final UserDao userDao;
    private final TransactionTemplate transactionTemplate;

    public UsersConnectionRepository(SocialConnectionDao socialConnectionDao, DSLContext dslContext,
                                     AmazonS3Service s3Service, UserDao userDao, TransactionTemplate transactionTemplate) {
        this.socialConnectionDao = socialConnectionDao;
        this.dslContext = dslContext;
        this.s3Service = s3Service;
        this.userDao = userDao;
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    public List<String> findUserIdsWithConnection(final Connection<?> connection) {
        SocialConnection socialConnection = socialConnectionDao.findById(
                dslContext.newRecord(Tables.SOCIAL_CONNECTION.PROVIDER_ID, Tables.SOCIAL_CONNECTION.PROVIDER_USER_ID)
                        .values(connection.getKey().getProviderId(), connection.getKey().getProviderUserId())
        );
        if (socialConnection == null) {
            Object api = connection.getApi();
            if (api instanceof Facebook) {
                Facebook facebook = (Facebook) api;
                String[] fields = {"email", "first_name", "last_name"};
                User userProfile = facebook.fetchObject("me", User.class, fields);
                if (dslContext.fetchExists(dslContext.selectOne().from(Tables.USER).where(Tables.USER.EMAIL.eq(userProfile.getEmail())))) {
                    return ImmutableList.of("email_exists");
                }
                byte[] userProfileImage = facebook.userOperations().getUserProfileImage(230, 230);
                String userId = UUID.randomUUID().toString();
                transactionTemplate.execute(status -> {
                    userDao.insert(new com.likemind.db.model.tables.pojos.User(
                            userId,
                            StringUtils.isBlank(userProfile.getEmail()) ? UUID.randomUUID().toString() : userProfile.getEmail(),
                            userProfile.getFirstName(),
                            userProfile.getLastName(),
                            UUID.randomUUID().toString(),
                            new String[0],
                            new String[0],
                            null,
                            null,
                            null,
                            UserRole.USER,
                            s3Service.uploadProfileLogo(userId, userProfileImage),
                            Ints.checkedCast(Instant.now().getEpochSecond())
                    ));
                    socialConnectionDao.insert(new SocialConnection(
                            connection.getKey().getProviderId(),
                            connection.getKey().getProviderUserId(),
                            userId
                    ));
                    return null;
                });
                return ImmutableList.of(userId);
            }
            throw new ApiException(connection.getKey().getProviderId(), "API type is not recognized");
        }
        return ImmutableList.of(socialConnection.getAppUserId());
    }

    @Override
    public Set<String> findUserIdsConnectedTo(final String providerId, final Set<String> providerUserIds) {
        return new HashSet<>(dslContext.select(Tables.SOCIAL_CONNECTION.APP_USER_ID)
                .from(Tables.SOCIAL_CONNECTION)
                .where(Tables.SOCIAL_CONNECTION.PROVIDER_ID.eq(providerId))
                .and(Tables.SOCIAL_CONNECTION.PROVIDER_USER_ID.in(providerUserIds))
                .fetchInto(String.class));
    }

    @Override
    public ConnectionRepository createConnectionRepository(final String userId) {
        return new ConnectionRepository() {
            @Override
            public MultiValueMap<String, Connection<?>> findAllConnections() {
                return null;
            }

            @Override
            public List<Connection<?>> findConnections(String providerId) {
                return null;
            }

            @Override
            public <A> List<Connection<A>> findConnections(Class<A> apiType) {
                return null;
            }

            @Override
            public MultiValueMap<String, Connection<?>> findConnectionsToUsers(MultiValueMap<String, String> providerUserIds) {
                return null;
            }

            @Override
            public Connection<?> getConnection(ConnectionKey connectionKey) {
                return null;
            }

            @Override
            public <A> Connection<A> getConnection(Class<A> apiType, String providerUserId) {
                return null;
            }

            @Override
            public <A> Connection<A> getPrimaryConnection(Class<A> apiType) {
                return null;
            }

            @Override
            public <A> Connection<A> findPrimaryConnection(Class<A> apiType) {
                return null;
            }

            @Override
            public void addConnection(Connection<?> connection) {

            }

            @Override
            public void updateConnection(Connection<?> connection) {

            }

            @Override
            public void removeConnections(String providerId) {

            }

            @Override
            public void removeConnection(ConnectionKey connectionKey) {

            }
        };
    }
}
