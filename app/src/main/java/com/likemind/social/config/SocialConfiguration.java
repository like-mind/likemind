package com.likemind.social.config;

import java.util.Objects;

import org.jooq.DSLContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.social.UserIdSource;
import org.springframework.social.config.annotation.ConnectionFactoryConfigurer;
import org.springframework.social.config.annotation.EnableSocial;
import org.springframework.social.config.annotation.SocialConfigurer;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.web.ProviderSignInController;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.security.AuthenticationNameUserIdSource;
import org.springframework.transaction.support.TransactionTemplate;

import com.likemind.db.model.tables.daos.SocialConnectionDao;
import com.likemind.db.model.tables.daos.UserDao;
import com.likemind.jooq.custom.UserRole;
import com.likemind.s3.AmazonS3Service;
import com.likemind.social.repo.UsersConnectionRepository;

@Configuration
@EnableSocial
public class SocialConfiguration implements SocialConfigurer {

    private final SocialConnectionDao socialConnectionDao;
    private final DSLContext dslContext;
    private final AmazonS3Service s3Service;
    private final UserDao userDao;
    private final TransactionTemplate transactionTemplate;

    public SocialConfiguration(SocialConnectionDao socialConnectionDao, DSLContext dslContext,
                               AmazonS3Service s3Service, UserDao userDao, TransactionTemplate transactionTemplate) {
        this.socialConnectionDao = socialConnectionDao;
        this.dslContext = dslContext;
        this.s3Service = s3Service;
        this.userDao = userDao;
        this.transactionTemplate = transactionTemplate;
    }

    @Override
    public void addConnectionFactories(ConnectionFactoryConfigurer connectionFactoryConfigurer, Environment environment) {
        connectionFactoryConfigurer.addConnectionFactory(new FacebookConnectionFactory(
                Objects.requireNonNull(environment.getProperty("spring.social.facebook.app-id")),
                Objects.requireNonNull(environment.getProperty("spring.social.facebook.app-secret"))));
    }

    @Override
    public UserIdSource getUserIdSource() {
        return new AuthenticationNameUserIdSource();
    }

    @Override
    public UsersConnectionRepository getUsersConnectionRepository(ConnectionFactoryLocator connectionFactoryLocator) {
        return new UsersConnectionRepository(socialConnectionDao, dslContext, s3Service, userDao, transactionTemplate);
    }

    @Bean
    public ProviderSignInController providerSignInController(ConnectionFactoryLocator connectionFactoryLocator,
                                                             org.springframework.social.connect.UsersConnectionRepository usersConnectionRepository) {
        return new ProviderSignInController(
                connectionFactoryLocator,
                usersConnectionRepository,
                (userId, connection, request) -> {
                    if ("email_exists".equals(userId)) {
                        return "/signin?error=email_exists";
                    }
                    User user = new User(userId, "", UserRole.USER.authorities);
                    user.eraseCredentials();
                    SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(
                            user,
                            null,
                            UserRole.USER.authorities
                    ));
                    return null;
                });
    }
}
