package com.likemind.chat.dto;

import com.likemind.db.model.tables.pojos.ChatMessage;
import com.likemind.db.model.tables.pojos.User;
import com.likemind.user.dto.BriefUserDto;

public class ChatMessageDto {

    public String id;

    public String message;

    public BriefUserDto sender;

    public Integer sentTimestamp;

    public ChatMessageDto(ChatMessage chatMessage, User sender) {
        this.id = chatMessage.getId();
        this.message = chatMessage.getMessage();
        this.sender = new BriefUserDto(sender);
        this.sentTimestamp = chatMessage.getSentTimestamp();
    }
}
