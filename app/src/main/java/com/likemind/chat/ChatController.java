package com.likemind.chat;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.security.auth.login.CredentialNotFoundException;

import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Ints;
import com.likemind.chat.dto.ChatMessageDto;
import com.likemind.db.model.Tables;
import com.likemind.db.model.tables.daos.ChatMessageDao;
import com.likemind.db.model.tables.daos.NotificationDao;
import com.likemind.db.model.tables.daos.PrivateChatDao;
import com.likemind.db.model.tables.daos.TeamDao;
import com.likemind.db.model.tables.daos.UserDao;
import com.likemind.db.model.tables.pojos.ChatMessage;
import com.likemind.db.model.tables.pojos.Notification;
import com.likemind.db.model.tables.pojos.PrivateChat;
import com.likemind.db.model.tables.pojos.Team;
import com.likemind.db.model.tables.pojos.User;
import com.likemind.jooq.custom.NotificationContent;
import com.likemind.web.error.BadRequestException;

@RequestMapping("/api/chat")
@RestController
public class ChatController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChatController.class);

    private final DSLContext dslContext;
    private final TeamDao teamDao;
    private final ChatMessageDao chatMessageDao;
    private final PrivateChatDao privateChatDao;
    private final UserDao userDao;
    private final NotificationDao notificationDao;
    private final TransactionTemplate transactionTemplate;

    public ChatController(DSLContext dslContext, TeamDao teamDao, ChatMessageDao chatMessageDao,
                          PrivateChatDao privateChatDao, UserDao userDao, NotificationDao notificationDao,
                          TransactionTemplate transactionTemplate) {
        this.dslContext = dslContext;
        this.teamDao = teamDao;
        this.chatMessageDao = chatMessageDao;
        this.privateChatDao = privateChatDao;
        this.userDao = userDao;
        this.notificationDao = notificationDao;
        this.transactionTemplate = transactionTemplate;
    }

    @GetMapping("/{chatId}")
    public Map<String, List<ChatMessageDto>> getMessagesPage(@PathVariable("chatId") String chatId,
                                                             @RequestParam(value = "lastSentTimestamp", required = false) Integer lastSentTimestamp,
                                                             @RequestParam(value = "lastId", required = false) String lastId,
                                                             @RequestParam(value = "count", defaultValue = "10") int count) {
        List<ChatMessage> messagesPage = lastSentTimestamp != null && lastId != null ?
                dslContext.selectFrom(Tables.CHAT_MESSAGE)
                        .where(Tables.CHAT_MESSAGE.CHAT_ID.eq(chatId))
                        .orderBy(Tables.CHAT_MESSAGE.SENT_TIMESTAMP.desc(), Tables.CHAT_MESSAGE.ID.desc())
                        .seekAfter(lastSentTimestamp, lastId).limit(count).fetchInto(ChatMessage.class) :
                dslContext.selectFrom(Tables.CHAT_MESSAGE)
                        .where(Tables.CHAT_MESSAGE.CHAT_ID.eq(chatId))
                        .orderBy(Tables.CHAT_MESSAGE.SENT_TIMESTAMP.desc(), Tables.CHAT_MESSAGE.ID.desc())
                        .limit(count).fetchInto(ChatMessage.class);
        String[] senderIds = messagesPage.stream().map(ChatMessage::getSenderId).toArray(String[]::new);
        Map<String, User> senders = userDao.fetchById(senderIds).stream().collect(Collectors.toMap(User::getId, Function.identity()));
        return ImmutableMap.of(
                "messages",
                messagesPage.stream().map(message -> {
                    User sender = senders.get(message.getSenderId());
                    if (sender == null) {
                        throw new IllegalStateException("User with ID " + message.getSenderId() + " has not been found unexpectedly");
                    }
                    return new ChatMessageDto(message, sender);
                }).collect(Collectors.toList())
        );
    }

    @PostMapping("/team/{teamId}")
    public Map<String, String> addTeamMessage(@PathVariable("teamId") String teamId,
                                              @RequestBody String message,
                                              @AuthenticationPrincipal UserDetails principal) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Team team = teamDao.findById(teamId);
        if (team == null) {
            throw new BadRequestException("Team with ID " + teamId + " cannot be found");
        }
        ChatMessage chatMessage = new ChatMessage(
                UUID.randomUUID().toString(),
                message,
                user.getId(),
                Ints.checkedCast(Instant.now().getEpochSecond()),
                teamId
        );
        transactionTemplate.execute(status -> {
            chatMessageDao.insert(chatMessage);
            String[] teamMembers = teamDao.findById(teamId).getMembers();
            for (String member : teamMembers) {
                if (!user.getId().equals(member)) {
                    try {
                        notificationDao.insert(new Notification(
                                UUID.randomUUID().toString(),
                                new NotificationContent.TeamChatMessage(
                                        user.getFirstName(),
                                        user.getLastName(),
                                        teamId
                                ),
                                member,
                                Ints.checkedCast(Instant.now().getEpochSecond()),
                                false
                        ));
                    } catch (Exception e) {
                        LOGGER.warn("Notification about team message in team '" + teamId + "'is not saved for user " + user.getId());
                    }
                }
            }
            return null;
        });
        return ImmutableMap.of("id", chatMessage.getId());
    }

    @PostMapping("/private/{chatId}")
    public Map<String, String> addPrivateMessage(@PathVariable("chatId") String chatId,
                                                 @RequestBody String message,
                                                 @AuthenticationPrincipal UserDetails principal) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        PrivateChat privateChat = privateChatDao.findById(chatId);
        if (privateChat == null) {
            throw new BadRequestException("Private chat with ID " + chatId + " cannot be found");
        }
        ChatMessage chatMessage = new ChatMessage(
                UUID.randomUUID().toString(),
                message,
                user.getId(),
                Ints.checkedCast(Instant.now().getEpochSecond()),
                chatId
        );
        transactionTemplate.execute(status -> {
            chatMessageDao.insert(chatMessage);
            String currentUserId = user.getId();
            String receiverId;
            if (privateChat.getUserId().equals(currentUserId)) {
                receiverId = privateChat.getAnotherUserId();
            } else {
                receiverId = privateChat.getUserId();
            }
            try {
                notificationDao.insert(new Notification(
                        UUID.randomUUID().toString(),
                        new NotificationContent.PrivateChatMessage(
                                user.getFirstName(),
                                user.getLastName(),
                                privateChat.getId()
                        ),
                        receiverId,
                        Ints.checkedCast(Instant.now().getEpochSecond()),
                        false
                ));
            } catch (Exception e) {
                LOGGER.warn("Notification about private message in chat '" + chatId + "'is not saved for user " + receiverId);
            }
            return null;
        });
        return ImmutableMap.of("id", chatMessage.getId());
    }

    @PutMapping("/private/{receiverId}/init")
    public Map<String, String> initPrivateChat(@AuthenticationPrincipal UserDetails principal,
                                               @PathVariable("receiverId") String receiverId) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        String currentUserId = user.getId();
        if (currentUserId.equals(receiverId)) {
            throw new BadRequestException("User cannot create a chat with himself");
        }
        PrivateChat privateChat = dslContext.selectFrom(Tables.PRIVATE_CHAT).where(Tables.PRIVATE_CHAT.USER_ID.eq(currentUserId))
                .and(Tables.PRIVATE_CHAT.ANOTHER_USER_ID.eq(receiverId))
                .fetchOneInto(PrivateChat.class);
        if (privateChat == null) {
            privateChat = dslContext.selectFrom(Tables.PRIVATE_CHAT).where(Tables.PRIVATE_CHAT.USER_ID.eq(receiverId))
                    .and(Tables.PRIVATE_CHAT.ANOTHER_USER_ID.eq(currentUserId))
                    .fetchOneInto(PrivateChat.class);
        }
        if (privateChat == null) {
            privateChat = new PrivateChat(UUID.randomUUID().toString(), currentUserId, receiverId);
            privateChatDao.insert(privateChat);
        }
        return ImmutableMap.of("id", privateChat.getId());
    }
}
