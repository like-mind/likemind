package com.likemind.team.dto;

import java.util.LinkedHashSet;

import com.likemind.db.model.tables.pojos.Team;
import com.likemind.user.dto.BriefUserDto;

public class TeamDto {

    public String projectId;

    public String name;

    public LinkedHashSet<BriefUserDto> members;

    public TeamDto(Team team, LinkedHashSet<BriefUserDto> members) {
        this.projectId = team.getProjectId();
        this.name = team.getName();
        this.members = members;
    }
}
