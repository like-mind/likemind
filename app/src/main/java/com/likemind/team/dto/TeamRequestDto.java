package com.likemind.team.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;

import com.likemind.db.model.tables.pojos.TeamRequest;
import com.likemind.user.dto.BriefUserDto;

public class TeamRequestDto {

    @NotBlank
    public String coverLetter;

    @Null
    public BriefUserDto appliedUser;

    public TeamRequestDto() {
    }

    public TeamRequestDto(TeamRequest teamRequest, BriefUserDto userDto) {
        this.coverLetter = teamRequest.getCoverLetter();
        this.appliedUser = userDto;
    }
}
