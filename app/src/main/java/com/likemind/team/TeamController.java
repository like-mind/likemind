package com.likemind.team;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.security.auth.login.CredentialNotFoundException;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.likemind.db.model.tables.daos.TeamDao;
import com.likemind.db.model.tables.daos.UserDao;
import com.likemind.db.model.tables.pojos.Team;
import com.likemind.db.model.tables.pojos.User;
import com.likemind.team.dto.TeamDto;
import com.likemind.user.dto.BriefUserDto;
import com.likemind.web.error.BadRequestException;

@RequestMapping("/api/teams")
@RestController
public class TeamController {

    private final TeamDao teamDao;
    private final UserDao userDao;

    public TeamController(TeamDao teamDao, UserDao userDao) {
        this.teamDao = teamDao;
        this.userDao = userDao;
    }

    @GetMapping("/{id}")
    public TeamDto getTeam(@AuthenticationPrincipal UserDetails principal,
                           @PathVariable("id") String id) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Team team = teamDao.findById(id);
        if (team == null) {
            throw new BadRequestException("Team with ID " + id + " cannot be found");
        }
        if (Arrays.stream(team.getMembers()).noneMatch(member -> member.equals(user.getId()))) {
            throw new BadRequestException("User with ID " + user.getId() + " is not member of team with ID " + team.getProjectId());
        }
        Map<String, User> users = userDao.fetchById(team.getMembers()).stream().collect(Collectors.toMap(User::getId, Function.identity()));
        return new TeamDto(team, Arrays.stream(team.getMembers()).map(member -> {
            User memberUser = users.get(member);
            if (memberUser == null) {
                throw new IllegalStateException("User with ID " + member + " has not been found unexpectedly");
            }
            return new BriefUserDto(memberUser);
        }).collect(Collectors.toCollection(LinkedHashSet::new)));
    }
}
