package com.likemind.user.dto;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;

import com.likemind.db.model.tables.pojos.User;
import com.likemind.jooq.custom.UserAvailability;
import com.likemind.user.validate.AtLeastOneParamPresent;
import com.likemind.user.validate.PasswordsMatch;
import com.likemind.user.validate.UserCreate;
import com.likemind.user.validate.UserUpdate;

@AtLeastOneParamPresent(groups = UserUpdate.class)
@PasswordsMatch(groups = {UserCreate.class, UserUpdate.class})
public class UserDto {

    @Null
    public String id;

    @NotBlank(groups = UserCreate.class)
    public String firstName;

    @NotBlank(groups = UserCreate.class)
    public String lastName;

    @NotBlank(groups = UserCreate.class)
    @Email(groups = {UserCreate.class, UserUpdate.class})
    public String email;

    @NotBlank(groups = UserCreate.class)
    public String password;

    @NotBlank(groups = UserCreate.class)
    public String passwordConfirmation;

    public LinkedHashSet<String> skills;

    public LinkedHashSet<String> resources;

    public UserAvailability availability;

    public String country;

    public String city;

    @Null
    public String profileLogoUrl;

    @Null
    public List<String> appliedProjects;

    public UserDto() {
    }

    public UserDto(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.email = user.getEmail();
        this.skills = Arrays.stream(user.getSkills()).collect(Collectors.toCollection(LinkedHashSet::new));
        this.resources = Arrays.stream(user.getResources()).collect(Collectors.toCollection(LinkedHashSet::new));
        this.availability = user.getAvailability();
        this.country = user.getCountry();
        this.city = user.getCity();
        this.profileLogoUrl = user.getProfileLogoUrl();
    }
}
