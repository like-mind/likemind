package com.likemind.user.dto;

import com.likemind.db.model.tables.pojos.User;

public class BriefUserDto {

    public String id;

    public String firstName;

    public String lastName;

    public BriefUserDto(User user) {
        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
    }
}