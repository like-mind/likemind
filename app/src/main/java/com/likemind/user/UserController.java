package com.likemind.user;

import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.security.auth.login.CredentialNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.jooq.DSLContext;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Ints;
import com.likemind.db.model.Tables;
import com.likemind.db.model.tables.daos.ProjectDao;
import com.likemind.db.model.tables.daos.TeamRequestDao;
import com.likemind.db.model.tables.daos.UserDao;
import com.likemind.db.model.tables.pojos.Project;
import com.likemind.db.model.tables.pojos.TeamRequest;
import com.likemind.db.model.tables.pojos.User;
import com.likemind.jooq.custom.UserRole;
import com.likemind.s3.AmazonS3Service;
import com.likemind.search.JestSearchService;
import com.likemind.user.dto.UserDto;
import com.likemind.user.validate.UserCreate;
import com.likemind.user.validate.UserUpdate;
import com.likemind.web.error.BadRequestException;

@RequestMapping("/api/users")
@RestController
public class UserController {

    private final JestSearchService searchService;
    private final AmazonS3Service s3Service;
    private final UserDao userDao;
    private final ProjectDao projectDao;
    private final TeamRequestDao teamRequestDao;
    private final DSLContext dslContext;
    private final BCryptPasswordEncoder passwordEncoder;
    private final TransactionTemplate transactionTemplate;

    public UserController(JestSearchService searchService, AmazonS3Service s3Service, UserDao userDao, ProjectDao projectDao,
                          TeamRequestDao teamRequestDao, DSLContext dslContext, BCryptPasswordEncoder passwordEncoder,
                          TransactionTemplate transactionTemplate) {
        this.searchService = searchService;
        this.s3Service = s3Service;
        this.userDao = userDao;
        this.projectDao = projectDao;
        this.teamRequestDao = teamRequestDao;
        this.dslContext = dslContext;
        this.passwordEncoder = passwordEncoder;
        this.transactionTemplate = transactionTemplate;
    }

    @PostMapping("/current")
    public UserDto getCurrentUser(@AuthenticationPrincipal UserDetails principal) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        UserDto userDto = new UserDto(user);
        userDto.appliedProjects = teamRequestDao.fetchByUserId(user.getId()).stream()
                .map(TeamRequest::getProjectId)
                .collect(Collectors.toList());
        return userDto;
    }

    @PostMapping
    public Map<String, String> createUser(@RequestBody @Validated(UserCreate.class) UserDto userRequest) {
        if (dslContext.fetchExists(dslContext.selectOne().from(Tables.USER).where(Tables.USER.EMAIL.eq(userRequest.email)))) {
            throw new BadRequestException("User with such an email already exists");
        }
        User user = new User(
                UUID.randomUUID().toString(),
                userRequest.email,
                userRequest.firstName,
                userRequest.lastName,
                passwordEncoder.encode(userRequest.password),
                userRequest.skills == null ? new String[0] : userRequest.skills.toArray(String[]::new),
                userRequest.resources == null ? new String[0] : userRequest.resources.toArray(String[]::new),
                userRequest.availability,
                userRequest.country,
                userRequest.city,
                UserRole.USER,
                null,
                Ints.checkedCast(Instant.now().getEpochSecond())
        );
        transactionTemplate.execute(status -> {
            userDao.insert(user);
            searchService.indexUser(user);
            return null;
        });
        return ImmutableMap.of("id", user.getId());
    }

    @PutMapping("/current")
    public Map<String, String> updateCurrentUser(@AuthenticationPrincipal UserDetails principal,
                                                 @RequestBody @Validated(UserUpdate.class) UserDto userRequest) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        if (StringUtils.isNotBlank(userRequest.email)) {
            user.setEmail(userRequest.email);
        }
        if (StringUtils.isNotBlank(userRequest.firstName)) {
            user.setFirstName(userRequest.firstName);
        }
        if (StringUtils.isNotBlank(userRequest.lastName)) {
            user.setLastName(userRequest.lastName);
        }
        if (StringUtils.isNotBlank(userRequest.password)) {
            user.setPasswordHash(passwordEncoder.encode(userRequest.password));
        }
        if (userRequest.availability != null) {
            user.setAvailability(userRequest.availability);
        }
        if (userRequest.skills != null) {
            user.setSkills(userRequest.skills.toArray(String[]::new));
        }
        if (userRequest.resources != null) {
            user.setResources(userRequest.resources.toArray(String[]::new));
        }
        if (userRequest.country != null) {
            user.setCountry(userRequest.country);
        }
        if (userRequest.city != null) {
            user.setCity(userRequest.city);
        }
        transactionTemplate.execute(status -> {
            userDao.update(user);
            List<Project> projects = projectDao.fetchByAuthorId(user.getId());
            searchService.updateUser(user);
            for (Project project : projects) {
                searchService.updateProjectAuthor(project.getId(), user);
            }
            return null;
        });
        return ImmutableMap.of("id", user.getId());
    }

    @PutMapping("/profile/logo")
    public Map<String, String> profileLogoUpload(@AuthenticationPrincipal UserDetails principal,
                                                 @RequestParam("image") MultipartFile file) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        user.setProfileLogoUrl(s3Service.uploadProfileLogo(user.getId(), file));
        userDao.update(user);
        return ImmutableMap.of("url", user.getProfileLogoUrl());
    }
}
