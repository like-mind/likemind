package com.likemind.user.validate;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = AtLeastOneParamPresentValidator.class)
@Documented
public @interface AtLeastOneParamPresent {

    String message() default "At least one parameter must be specified for update";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
