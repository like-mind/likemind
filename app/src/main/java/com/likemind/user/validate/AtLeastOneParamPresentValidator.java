package com.likemind.user.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.likemind.user.dto.UserDto;

public class AtLeastOneParamPresentValidator implements ConstraintValidator<AtLeastOneParamPresent, UserDto> {

    public void initialize(AtLeastOneParamPresent constraint) {
    }

    public boolean isValid(UserDto userUpdateRequest, ConstraintValidatorContext context) {
        return StringUtils.isNotBlank(userUpdateRequest.email)
                || StringUtils.isNotBlank(userUpdateRequest.firstName)
                || StringUtils.isNotBlank(userUpdateRequest.lastName)
                || StringUtils.isNotBlank(userUpdateRequest.password)
                || StringUtils.isNotBlank(userUpdateRequest.passwordConfirmation)
                || userUpdateRequest.country != null
                || userUpdateRequest.city != null
                || userUpdateRequest.availability != null
                || userUpdateRequest.skills != null
                || userUpdateRequest.resources != null;
    }
}
