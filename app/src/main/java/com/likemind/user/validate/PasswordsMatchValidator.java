package com.likemind.user.validate;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.likemind.user.dto.UserDto;

public class PasswordsMatchValidator implements ConstraintValidator<PasswordsMatch, UserDto> {

    public void initialize(PasswordsMatch constraint) {
    }

    public boolean isValid(UserDto userRequest, ConstraintValidatorContext context) {
        return StringUtils.equals(userRequest.password, userRequest.passwordConfirmation);
    }
}
