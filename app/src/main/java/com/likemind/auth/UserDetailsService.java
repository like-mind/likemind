package com.likemind.auth;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.likemind.db.model.Tables;
import com.likemind.db.model.tables.daos.UserDao;
import com.likemind.db.model.tables.pojos.User;

@Service
public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {

    private final UserDao userDao;

    public UserDetailsService(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userDao.fetchOne(Tables.USER.EMAIL, email);
        if (user == null) {
            throw new UsernameNotFoundException(String.format("User with email %s is not found in the database", email));
        }
        Collection<? extends GrantedAuthority> authorities = user.getRole().authorities;
        return new org.springframework.security.core.userdetails.User(user.getId(), user.getPasswordHash(), authorities);
    }
}
