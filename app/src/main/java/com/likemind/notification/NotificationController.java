package com.likemind.notification;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.security.auth.login.CredentialNotFoundException;

import org.jooq.DSLContext;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;
import com.likemind.db.model.Tables;
import com.likemind.db.model.tables.daos.NotificationDao;
import com.likemind.db.model.tables.daos.UserDao;
import com.likemind.db.model.tables.pojos.Notification;
import com.likemind.db.model.tables.pojos.User;
import com.likemind.notification.dto.NotificationDto;
import com.likemind.web.error.BadRequestException;

@RequestMapping("/api/notifications")
@RestController
public class NotificationController {

    private final UserDao userDao;
    private final NotificationDao notificationDao;
    private final DSLContext dslContext;

    public NotificationController(UserDao userDao, NotificationDao notificationDao, DSLContext dslContext) {
        this.userDao = userDao;
        this.notificationDao = notificationDao;
        this.dslContext = dslContext;
    }

    @GetMapping
    public Map<String, List<NotificationDto>> getNotifications(@AuthenticationPrincipal UserDetails principal) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        List<Notification> notifications = dslContext.selectFrom(Tables.NOTIFICATION)
                .where(Tables.NOTIFICATION.RECEIVER_ID.eq(user.getId()))
                .and(Tables.NOTIFICATION.PROCESSED.eq(false))
                .fetchInto(Notification.class);
        return ImmutableMap.of("notifications",
                notifications.stream().map(NotificationDto::new).collect(Collectors.toList()));
    }

    @PostMapping("/{notificationId}")
    public Map<String, String> processNotification(@PathVariable("notificationId") String notificationId,
                                                   @AuthenticationPrincipal UserDetails principal) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Notification notification = notificationDao.findById(notificationId);
        if (notification == null) {
            throw new BadRequestException("Notification with ID " + notificationId + " cannot be found");
        }
        if (!notification.getReceiverId().equals(user.getId())) {
            throw new BadRequestException("Current user is not allowed to process notification with ID " + notificationId);
        }
        notification.setProcessed(true);
        notificationDao.update(notification);
        return ImmutableMap.of("id", notificationId);
    }
}
