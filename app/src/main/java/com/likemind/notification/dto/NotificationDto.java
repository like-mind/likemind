package com.likemind.notification.dto;

import com.likemind.db.model.tables.pojos.Notification;
import com.likemind.jooq.custom.NotificationContent;

public class NotificationDto {

    public String id;

    public NotificationContent content;

    public NotificationDto(Notification notification) {
        this.id = notification.getId();
        this.content = notification.getContent();
    }
}
