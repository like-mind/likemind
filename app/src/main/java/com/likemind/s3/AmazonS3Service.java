package com.likemind.s3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.amazonaws.services.s3.transfer.model.UploadResult;
import com.likemind.s3.error.S3Exception;

@Service
public class AmazonS3Service {

    private static final Logger LOGGER = LoggerFactory.getLogger(AmazonS3Service.class);

    @Value("${transfer.s3-bucket}")
    private String s3Bucket;
    private final TransferManager transferManager;

    public AmazonS3Service(AmazonS3 amazonS3) {
        this.transferManager = TransferManagerBuilder.standard().withS3Client(amazonS3).build();
    }

    public String uploadProfileLogo(String userId, InputStream imageFileStream, long imageSize) {
        try {
            ObjectMetadata imageMetadata = new ObjectMetadata();
            imageMetadata.setContentLength(imageSize);
            imageMetadata.setContentType("image/png");
            String s3Key = String.format("users/%s/profile/logo.png", userId);
            PutObjectRequest imageUploadRequest = new PutObjectRequest(s3Bucket, s3Key, imageFileStream, imageMetadata)
                    .withCannedAcl(CannedAccessControlList.PublicRead);
            Upload upload = transferManager.upload(imageUploadRequest);
            UploadResult uploadResult = upload.waitForUploadResult();
            return String.format("https://%s.s3.amazonaws.com/%s?versionId=%s", s3Bucket, s3Key, uploadResult.getVersionId());
        } catch (InterruptedException e) {
            throw new S3Exception(e);
        }
    }

    public String uploadProfileLogo(String userId, MultipartFile multipartFile) {
        InputStream imageInputStream = null;
        try {
            imageInputStream = multipartFile.getInputStream();
            return uploadProfileLogo(userId, imageInputStream, multipartFile.getSize());
        } catch (IOException e) {
            throw new S3Exception(e);
        } finally {
            if (imageInputStream != null) {
                try {
                    imageInputStream.close();
                } catch (IOException e) {
                    LOGGER.warn("Error while closing the image input stream: " + e.getMessage(), e);
                }
            }
        }
    }

    public String uploadProfileLogo(String userId, byte[] imageBytes) {
        return uploadProfileLogo(userId, new ByteArrayInputStream(imageBytes), imageBytes.length);
    }
}
