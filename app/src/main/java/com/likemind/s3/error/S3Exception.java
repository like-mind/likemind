package com.likemind.s3.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "File transfer error")
public class S3Exception extends RuntimeException {

    public S3Exception(Throwable cause) {
        super(cause.getMessage(), cause);
    }
}
