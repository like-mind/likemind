package com.likemind.search.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.likemind.db.model.tables.pojos.Project;
import com.likemind.db.model.tables.pojos.User;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchProject {

    public String title;
    public String content;
    public String authorFirstName;
    public String authorLastName;
    public String[] categories;
    public String[] skills;
    public String[] resources;
    public String country;
    public String city;
    public String postalCode;

    public SearchProject(Project project, User author) {
        this.title = project.getTitle();
        this.content = project.getContent();
        this.authorFirstName = author.getFirstName();
        this.authorLastName = author.getLastName();
        this.categories = project.getCategories();
        this.skills = project.getSkills();
        this.resources = project.getResources();
        this.country = project.getCountry();
        this.city = project.getCity();
        this.postalCode = project.getPostalCode();
    }

    public SearchProject(Project project) {
        this.title = project.getTitle();
        this.content = project.getContent();
        this.categories = project.getCategories();
        this.skills = project.getSkills();
        this.resources = project.getResources();
        this.country = project.getCountry();
        this.city = project.getCity();
        this.postalCode = project.getPostalCode();
    }

    public SearchProject(User user) {
        this.authorFirstName = user.getFirstName();
        this.authorLastName = user.getLastName();
    }

    public static String[] getSearchFields() {
        return new String[]{"title", "content", "authorFirstName", "authorLastName", "categories", "skills", "resources"};
    }
}
