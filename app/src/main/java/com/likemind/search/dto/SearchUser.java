package com.likemind.search.dto;

import com.likemind.db.model.tables.pojos.User;

public class SearchUser {

    public String firstName;
    public String lastName;
    public String[] skills;
    public String[] resources;
    public String country;
    public String city;

    public SearchUser(User other) {
        this.firstName = other.getFirstName();
        this.lastName = other.getLastName();
        this.skills = other.getSkills();
        this.resources = other.getResources();
        this.country = other.getCountry();
        this.city = other.getCity();
    }

    public static String[] getSearchFields() {
        return new String[]{"firstName", "lastName", "skills", "resources"};
    }
}
