package com.likemind.search;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.elasticsearch.common.Strings;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.likemind.db.model.tables.pojos.Project;
import com.likemind.db.model.tables.pojos.User;
import com.likemind.jooq.custom.UserRole;
import com.likemind.search.dto.SearchProject;
import com.likemind.search.dto.SearchUser;
import com.likemind.search.error.SearchException;

import io.searchbox.client.JestClient;
import io.searchbox.core.Delete;
import io.searchbox.core.DocumentResult;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.Update;

@Service
public class JestSearchService {

    private static final String USERS_INDEX = "users";
    private static final String PROJECTS_INDEX = "projects";
    private static final String DEFAULT_PROJECT_TYPE = "default";
    private static final String DOC_FIELD = "doc";

    private final JestClient jestClient;
    private final ObjectMapper objectMapper;

    public JestSearchService(JestClient jestClient, ObjectMapper objectMapper) {
        this.jestClient = jestClient;
        this.objectMapper = objectMapper;
    }

    public void indexUser(User user) {
        try {
            Index indexAction = new Index.Builder(new SearchUser(user)).id(user.getId())
                    .index(USERS_INDEX).type(user.getRole().name()).build();
            DocumentResult result = jestClient.execute(indexAction);
            if (!result.isSucceeded()) {
                throw new SearchException(result.getErrorMessage());
            }
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    public void updateUser(User user) {
        try {
            XContentBuilder updateBuilder = XContentFactory.jsonBuilder()
                    .startObject()
                    .rawField(DOC_FIELD, new ByteArrayInputStream(objectMapper.writeValueAsBytes(new SearchUser(user))), XContentType.JSON)
                    .endObject();
            Update updateAction = new Update.Builder(Strings.toString(updateBuilder)).id(user.getId())
                    .index(USERS_INDEX).type(user.getRole().name()).build();
            DocumentResult result = jestClient.execute(updateAction);
            if (!result.isSucceeded()) {
                throw new SearchException(result.getErrorMessage());
            }
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    public void deleteUser(String userId, UserRole role) {
        try {
            Delete deleteAction = new Delete.Builder(userId).index(USERS_INDEX)
                    .type(role.name()).build();
            DocumentResult result = jestClient.execute(deleteAction);
            if (!result.isSucceeded()) {
                throw new SearchException(result.getErrorMessage());
            }
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    public List<String> searchUsers(String searchString) {
        try {
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(QueryBuilders.multiMatchQuery(searchString, SearchUser.getSearchFields()));
            searchSourceBuilder.fetchSource(false);
            Search usersSearch = new Search.Builder(searchSourceBuilder.toString())
                    .addIndex(USERS_INDEX).addType(UserRole.USER.name()).build();
            SearchResult searchResult = jestClient.execute(usersSearch);
            List<SearchResult.Hit<SearchUser, Void>> hits = searchResult.getHits(SearchUser.class);
            return hits.stream().map(hit -> hit.id).collect(Collectors.toList());
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    public void indexProject(Project project, User user) {
        try {
            Index indexAction = new Index.Builder(new SearchProject(project, user)).id(project.getId())
                    .index(PROJECTS_INDEX).type(DEFAULT_PROJECT_TYPE).build();
            DocumentResult result = jestClient.execute(indexAction);
            if (!result.isSucceeded()) {
                throw new SearchException(result.getErrorMessage());
            }
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    public void updateProject(Project project) {
        try {
            XContentBuilder updateBuilder = XContentFactory.jsonBuilder()
                    .startObject()
                    .rawField(DOC_FIELD, new ByteArrayInputStream(objectMapper.writeValueAsBytes(new SearchProject(project))), XContentType.JSON)
                    .endObject();
            Update updateAction = new Update.Builder(Strings.toString(updateBuilder)).id(project.getId())
                    .index(PROJECTS_INDEX).type(DEFAULT_PROJECT_TYPE).build();
            DocumentResult result = jestClient.execute(updateAction);
            if (!result.isSucceeded()) {
                throw new SearchException(result.getErrorMessage());
            }
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    public void updateProjectAuthor(String projectId, User user) {
        try {
            XContentBuilder updateBuilder = XContentFactory.jsonBuilder()
                    .startObject()
                    .rawField(DOC_FIELD, new ByteArrayInputStream(objectMapper.writeValueAsBytes(new SearchProject(user))), XContentType.JSON)
                    .endObject();
            Update updateAction = new Update.Builder(Strings.toString(updateBuilder)).id(projectId)
                    .index(PROJECTS_INDEX).type(DEFAULT_PROJECT_TYPE).build();
            DocumentResult result = jestClient.execute(updateAction);
            if (!result.isSucceeded()) {
                throw new SearchException(result.getErrorMessage());
            }
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    public void deleteProject(String projectId) {
        try {
            Delete deleteAction = new Delete.Builder(projectId).index(PROJECTS_INDEX)
                    .type(DEFAULT_PROJECT_TYPE).build();
            DocumentResult result = jestClient.execute(deleteAction);
            if (!result.isSucceeded()) {
                throw new SearchException(result.getErrorMessage());
            }
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }

    public Set<String> searchProjects(String searchString) {
        try {
            SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
            searchSourceBuilder.query(QueryBuilders.multiMatchQuery(searchString, SearchProject.getSearchFields()));
            searchSourceBuilder.fetchSource(false);
            Search projectsSearch = new Search.Builder(searchSourceBuilder.toString())
                    .addIndex(PROJECTS_INDEX).addType(DEFAULT_PROJECT_TYPE).build();
            SearchResult searchResult = jestClient.execute(projectsSearch);
            if (searchResult.isSucceeded()) {
                List<SearchResult.Hit<SearchProject, Void>> hits = searchResult.getHits(SearchProject.class);
                return hits.stream().map(hit -> hit.id).collect(Collectors.toSet());
            }
            return Collections.emptySet();
        } catch (IOException e) {
            throw new SearchException(e);
        }
    }
}
