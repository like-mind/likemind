package com.likemind.search.error;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "Search error")
public class SearchException extends RuntimeException {

    public SearchException(Throwable cause) {
        super(cause.getMessage(), cause);
    }

    public SearchException(String errorMessage) {
        super(errorMessage);
    }
}
