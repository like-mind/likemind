package com.likemind.search.config;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.elasticsearch.jest.HttpClientConfigBuilderCustomizer;
import org.springframework.boot.autoconfigure.elasticsearch.jest.JestAutoConfiguration;
import org.springframework.boot.autoconfigure.elasticsearch.jest.JestProperties;
import org.springframework.boot.autoconfigure.gson.GsonAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.google.common.base.Supplier;
import com.google.gson.Gson;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import vc.inreach.aws.request.AWSSigner;
import vc.inreach.aws.request.AWSSigningRequestInterceptor;

@Profile("!dev")
@Configuration
@ConditionalOnClass(JestClient.class)
@EnableConfigurationProperties(JestProperties.class)
@AutoConfigureAfter(GsonAutoConfiguration.class)
@AutoConfigureBefore(JestAutoConfiguration.class)
public class JestAWSAutoConfiguration extends JestAutoConfiguration {

    private static final String ES_SERVICE_NAME = "es";

    public JestAWSAutoConfiguration(JestProperties properties, ObjectProvider<Gson> gson,
                                    ObjectProvider<HttpClientConfigBuilderCustomizer> builderCustomizers) {
        super(properties, gson, builderCustomizers);
    }

    @Bean(destroyMethod = "shutdownClient")
    public JestClient jestClient() {
        Region region = Regions.getCurrentRegion();
        DefaultAWSCredentialsProviderChain credentials = new DefaultAWSCredentialsProviderChain();
        Supplier<LocalDateTime> clock = () -> LocalDateTime.now(ZoneOffset.UTC);
        AWSSigner awsSigner = new AWSSigner(credentials, region.getName(), ES_SERVICE_NAME, clock);
        AWSSigningRequestInterceptor requestInterceptor = new AWSSigningRequestInterceptor(awsSigner);
        JestClientFactory factory = new JestClientFactory() {

            @Override
            protected HttpClientBuilder configureHttpClient(HttpClientBuilder builder) {
                builder.addInterceptorLast(requestInterceptor);
                return builder;
            }

            @Override
            protected HttpAsyncClientBuilder configureHttpClient(HttpAsyncClientBuilder builder) {
                builder.addInterceptorLast(requestInterceptor);
                return builder;
            }
        };
        factory.setHttpClientConfig(createHttpClientConfig());
        return factory.getObject();
    }
}
