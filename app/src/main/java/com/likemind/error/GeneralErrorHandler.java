package com.likemind.error;

import javax.security.auth.login.CredentialNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.likemind.web.error.BadRequestException;

@ControllerAdvice
public class GeneralErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(GeneralErrorHandler.class);

    @ExceptionHandler
    public void handleAllExceptions(Exception e) throws Exception {
        LOGGER.error(e.getMessage(), e);
        throw e;
    }

    @ExceptionHandler
    public ResponseEntity<String> handleBadRequestException(BadRequestException e) {
        LOGGER.error(e.getMessage(), e);
        return ResponseEntity.badRequest().body(e.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity<String> handleUnauthorizedExceptions(CredentialNotFoundException e) {
        LOGGER.error(e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
    }
}
