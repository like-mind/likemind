package com.likemind.project.dto;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Null;

import com.likemind.db.model.tables.pojos.Project;
import com.likemind.db.model.tables.pojos.User;
import com.likemind.project.validate.AtLeastOneParamPresent;
import com.likemind.project.validate.ProjectCreate;
import com.likemind.project.validate.ProjectUpdate;
import com.likemind.team.dto.TeamRequestDto;
import com.likemind.user.dto.BriefUserDto;

@AtLeastOneParamPresent(groups = ProjectUpdate.class)
public class ProjectDto {

    @Null
    public String id;

    @NotBlank(groups = ProjectCreate.class)
    public String title;

    @NotBlank(groups = ProjectCreate.class)
    public String content;

    @Null
    public BriefUserDto author;

    public LinkedHashSet<String> categories;

    public LinkedHashSet<String> skills;

    public LinkedHashSet<String> resources;

    public String country;

    public String city;

    public String postalCode;

    @Null
    public List<String> likedUsers;

    @Null
    public List<TeamRequestDto> teamRequests;

    @Null
    public Integer createdTimestamp;

    @Null
    public String teamId;

    public ProjectDto() {
    }

    public ProjectDto(Project project, User author) {
        this.id = project.getId();
        this.title = project.getTitle();
        this.content = project.getContent();
        this.categories = Arrays.stream(project.getCategories()).collect(Collectors.toCollection(LinkedHashSet::new));
        this.skills = Arrays.stream(project.getSkills()).collect(Collectors.toCollection(LinkedHashSet::new));
        this.resources = Arrays.stream(project.getResources()).collect(Collectors.toCollection(LinkedHashSet::new));
        this.createdTimestamp = project.getCreatedTimestamp();
        this.likedUsers = Arrays.asList(project.getLikedUsers());
        this.author = new BriefUserDto(author);
    }
}