package com.likemind.project;

import java.time.Instant;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.security.auth.login.CredentialNotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.jooq.DSLContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.collect.ImmutableMap;
import com.google.common.primitives.Ints;
import com.likemind.db.model.Tables;
import com.likemind.db.model.tables.daos.NotificationDao;
import com.likemind.db.model.tables.daos.ProjectDao;
import com.likemind.db.model.tables.daos.TeamDao;
import com.likemind.db.model.tables.daos.TeamRequestDao;
import com.likemind.db.model.tables.daos.UserDao;
import com.likemind.db.model.tables.pojos.Notification;
import com.likemind.db.model.tables.pojos.Project;
import com.likemind.db.model.tables.pojos.Team;
import com.likemind.db.model.tables.pojos.TeamRequest;
import com.likemind.db.model.tables.pojos.User;
import com.likemind.jooq.custom.NotificationContent;
import com.likemind.project.dto.ProjectDto;
import com.likemind.project.validate.ProjectCreate;
import com.likemind.project.validate.ProjectUpdate;
import com.likemind.search.JestSearchService;
import com.likemind.team.dto.TeamRequestDto;
import com.likemind.user.dto.BriefUserDto;
import com.likemind.web.error.BadRequestException;

// TODO: Add programattic transactions management.
@RequestMapping("/api/projects")
@RestController
public class ProjectController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectController.class);

    private final JestSearchService searchService;
    private final DSLContext dslContext;
    private final ProjectDao projectDao;
    private final UserDao userDao;
    private final TeamRequestDao teamRequestDao;
    private final TeamDao teamDao;
    private final NotificationDao notificationDao;
    private final TransactionTemplate transactionTemplate;

    public ProjectController(JestSearchService searchService, DSLContext dslContext, ProjectDao projectDao, UserDao userDao,
                             TeamRequestDao teamRequestDao, TeamDao teamDao, NotificationDao notificationDao,
                             TransactionTemplate transactionTemplate) {
        this.searchService = searchService;
        this.dslContext = dslContext;
        this.projectDao = projectDao;
        this.userDao = userDao;
        this.teamRequestDao = teamRequestDao;
        this.teamDao = teamDao;
        this.notificationDao = notificationDao;
        this.transactionTemplate = transactionTemplate;
    }

    @GetMapping("/intro")
    public Map<String, List<ProjectDto>> getIntroductoryProjects() {
        return getProjects(null, null, 10);
    }

    @GetMapping
    public Map<String, List<ProjectDto>> getProjects(@RequestParam(value = "lastCreatedTimestamp", required = false) Integer lastCreatedTimestamp,
                                                     @RequestParam(value = "lastId", required = false) String lastId,
                                                     @RequestParam(value = "count", defaultValue = "10") int count) {
        List<Project> projectsPage = lastCreatedTimestamp != null && lastId != null ?
                dslContext.selectFrom(Tables.PROJECT)
                        .orderBy(Tables.PROJECT.CREATED_TIMESTAMP.desc(), Tables.PROJECT.ID.desc())
                        .seekAfter(lastCreatedTimestamp, lastId).limit(count)
                        .fetchInto(Project.class) :
                dslContext.selectFrom(Tables.PROJECT)
                        .orderBy(Tables.PROJECT.CREATED_TIMESTAMP.desc(), Tables.PROJECT.ID.desc())
                        .limit(count).fetchInto(Project.class);
        String[] authorIds = projectsPage.stream().map(Project::getAuthorId).toArray(String[]::new);
        Map<String, User> authors = userDao.fetchById(authorIds)
                .stream().collect(Collectors.toMap(User::getId, Function.identity()));
        return ImmutableMap.of("projects", projectsPage.stream().map(project -> {
            User author = authors.get(project.getAuthorId());
            if (author == null) {
                throw new IllegalStateException("User with ID " + project.getAuthorId() + " has not been found unexpectedly");
            }
            return new ProjectDto(project, author);
        }).collect(Collectors.toList()));
    }

    @PostMapping
    public Map<String, String> createProject(@AuthenticationPrincipal UserDetails principal,
                                             @RequestBody @Validated(ProjectCreate.class) ProjectDto projectRequest) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Project project = new Project(
                UUID.randomUUID().toString(),
                projectRequest.title,
                projectRequest.content,
                user.getId(),
                projectRequest.categories == null ? new String[0] : projectRequest.categories.toArray(String[]::new),
                projectRequest.skills == null ? new String[0] : projectRequest.skills.toArray(String[]::new),
                projectRequest.resources == null ? new String[0] : projectRequest.resources.toArray(String[]::new),
                projectRequest.country,
                projectRequest.city,
                projectRequest.postalCode,
                new String[0],
                Ints.checkedCast(Instant.now().getEpochSecond())
        );
        transactionTemplate.execute(status -> {
            projectDao.insert(project);
            searchService.indexProject(project, user);
            return null;
        });
        return ImmutableMap.of("id", project.getId());
    }

    @PutMapping("/{projectId}")
    public Map<String, String> updateProject(@AuthenticationPrincipal UserDetails principal,
                                             @PathVariable("projectId") String projectId,
                                             @RequestBody @Validated(ProjectUpdate.class) ProjectDto projectRequest) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Project project = projectDao.findById(projectId);
        if (project == null) {
            throw new BadRequestException("Project with ID " + projectId + " cannot be found");
        }
        if (!user.getId().equals(project.getAuthorId())) {
            throw new AccessDeniedException("Current user is not allowed to modify project with ID " + project.getId());
        }
        if (StringUtils.isNotBlank(projectRequest.title)) {
            project.setTitle(projectRequest.title);
        }
        if (StringUtils.isNotBlank(projectRequest.content)) {
            project.setContent(projectRequest.content);
        }
        if (projectRequest.categories != null) {
            project.setCategories(projectRequest.categories.toArray(String[]::new));
        }
        if (projectRequest.skills != null) {
            project.setSkills(projectRequest.skills.toArray(String[]::new));
        }
        if (projectRequest.resources != null) {
            project.setResources(projectRequest.resources.toArray(String[]::new));
        }
        if (projectRequest.country != null) {
            project.setCountry(projectRequest.country);
        }
        if (projectRequest.city != null) {
            project.setCity(projectRequest.city);
        }
        if (projectRequest.postalCode != null) {
            project.setPostalCode(projectRequest.postalCode);
        }
        transactionTemplate.execute(status -> {
            projectDao.update(project);
            searchService.updateProject(project);
            return null;
        });
        return ImmutableMap.of("id", projectId);
    }

    @DeleteMapping("/{projectId}")
    public Map<String, String> deleteProject(@AuthenticationPrincipal UserDetails principal,
                                             @PathVariable("projectId") String projectId) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Project project = projectDao.findById(projectId);
        if (project == null) {
            throw new BadRequestException("Project with ID " + projectId + " cannot be found");
        }
        if (!user.getId().equals(project.getAuthorId())) {
            throw new AccessDeniedException("Current user is not allowed to remove project with ID " + project.getId());
        }
        transactionTemplate.execute(status -> {
            projectDao.delete(project);
            searchService.deleteProject(project.getId());
            return null;
        });
        return ImmutableMap.of("id", projectId);
    }

    @PutMapping("/{projectId}/likes")
    public Map<String, String> addLikedUser(@AuthenticationPrincipal UserDetails principal,
                                            @PathVariable("projectId") String projectId) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Project project = projectDao.findById(projectId);
        if (project == null) {
            throw new BadRequestException("Project with ID " + projectId + " cannot be found");
        }
        Set<String> likedUsers = Arrays.stream(project.getLikedUsers()).collect(Collectors.toCollection(LinkedHashSet::new));
        if (likedUsers.add(user.getId())) {
            project.setLikedUsers(likedUsers.toArray(String[]::new));
            projectDao.update(project);
        }
        return ImmutableMap.of("id", projectId);
    }

    @DeleteMapping("/{projectId}/likes")
    public Map<String, String> deleteLikedUser(@AuthenticationPrincipal UserDetails principal,
                                               @PathVariable("projectId") String projectId) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Project project = projectDao.findById(projectId);
        if (project == null) {
            throw new BadRequestException("Project with ID " + projectId + " cannot be found");
        }
        Set<String> likedUsers = Arrays.stream(project.getLikedUsers()).collect(Collectors.toCollection(LinkedHashSet::new));
        if (likedUsers.remove(user.getId())) {
            project.setLikedUsers(likedUsers.toArray(String[]::new));
            projectDao.update(project);
        }
        return ImmutableMap.of("id", projectId);
    }

    @GetMapping("/{id}")
    public ProjectDto getProject(@AuthenticationPrincipal UserDetails principal,
                                 @PathVariable("id") String id) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Project project = projectDao.findById(id);
        if (project == null) {
            throw new BadRequestException("Project with ID " + id + " cannot be found");
        }
        User author = userDao.findById(project.getAuthorId());
        if (author == null) {
            throw new IllegalStateException("User with ID " + project.getAuthorId() + " has not been found unexpectedly");
        }
        ProjectDto projectDto = new ProjectDto(project, author);
        if (user.getId().equals(project.getAuthorId())) {
            List<TeamRequest> teamRequests = dslContext.selectFrom(Tables.TEAM_REQUEST)
                    .where(Tables.TEAM_REQUEST.PROJECT_ID.eq(id))
                    .and(Tables.TEAM_REQUEST.PROCESSED.eq(false))
                    .fetchInto(TeamRequest.class);
            String[] teamRequestUserIds = teamRequests.stream().map(TeamRequest::getUserId).toArray(String[]::new);
            Map<String, User> users = userDao.fetchById(teamRequestUserIds).stream().collect(Collectors.toMap(User::getId, Function.identity()));
            projectDto.teamRequests = teamRequests.stream().map(teamRequest -> {
                User teamRequestUser = users.get(teamRequest.getUserId());
                if (teamRequestUser == null) {
                    throw new IllegalStateException("User with ID " + teamRequest.getUserId() + " has not been found unexpectedly");
                }
                return new TeamRequestDto(teamRequest, new BriefUserDto(teamRequestUser));
            }).collect(Collectors.toList());
        }
        Team team = teamDao.findById(id);
        if (team != null && Arrays.asList(team.getMembers()).contains(user.getId())) {
            projectDto.teamId = id;
        }
        return projectDto;
    }

    @PostMapping("/{projectId}/teams")
    public Map<String, String> applyToTeam(@AuthenticationPrincipal UserDetails principal,
                                           @PathVariable("projectId") String projectId,
                                           @RequestBody @Validated TeamRequestDto teamRequest) throws CredentialNotFoundException {
        User user = userDao.findById(principal.getUsername());
        if (user == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Project project = projectDao.findById(projectId);
        if (project == null) {
            throw new BadRequestException("Project with ID " + projectId + " cannot be found");
        }
        if (user.getId().equals(project.getAuthorId())) {
            throw new BadRequestException("Author of the project cannot apply to his own project team");
        }
        TeamRequest existingTR = teamRequestDao.findById(dslContext.newRecord(Tables.TEAM_REQUEST.PROJECT_ID, Tables.TEAM_REQUEST.USER_ID)
                .values(projectId, user.getId()));
        if (existingTR != null) {
            if (existingTR.getProcessed()) {
                throw new BadRequestException("Current user is already involved into project with ID " + projectId);
            } else {
                throw new BadRequestException("Current user has already applied to project with ID " + projectId);
            }
        }
        transactionTemplate.execute(status -> {
            teamRequestDao.insert(new TeamRequest(projectId, user.getId(), teamRequest.coverLetter, Ints.checkedCast(Instant.now().getEpochSecond()), false));
            try {
                notificationDao.insert(new Notification(
                        UUID.randomUUID().toString(),
                        new NotificationContent.TeamRequest(
                                user.getFirstName(),
                                user.getLastName(),
                                projectId
                        ),
                        project.getAuthorId(),
                        Ints.checkedCast(Instant.now().getEpochSecond()),
                        false
                ));
            } catch (Exception e) {
                LOGGER.warn("Notification about application to project '" + projectId + "'is not saved for user " + user.getId());
            }
            return null;
        });
        return ImmutableMap.of("id", projectId);
    }

    @PutMapping("/{projectId}/teams/users/{userId}")
    public Object approveTeamRequest(@AuthenticationPrincipal UserDetails principal,
                                     @PathVariable("projectId") String projectId,
                                     @PathVariable("userId") String userId) throws CredentialNotFoundException {
        User currentUser = userDao.findById(principal.getUsername());
        if (currentUser == null) {
            throw new CredentialNotFoundException("Credentials were not found unexpectedly");
        }
        Project project = projectDao.findById(projectId);
        if (project == null) {
            throw new BadRequestException("Project with ID " + projectId + " cannot be found");
        }
        if (!currentUser.getId().equals(project.getAuthorId())) {
            throw new AccessDeniedException("Current user is not allowed to approve team requests for project with ID "
                    + project.getId());
        }
        User user = userDao.findById(userId);
        if (user == null) {
            throw new BadRequestException("User with ID " + userId + " cannot be found");
        }
        TeamRequest teamRequest = teamRequestDao.findById(dslContext.newRecord(Tables.TEAM_REQUEST.PROJECT_ID, Tables.TEAM_REQUEST.USER_ID)
                .values(projectId, userId));
        if (teamRequest == null) {
            throw new BadRequestException("Team request for user with ID " + userId + " into project with ID " +
                    projectId + " cannot be found");
        }
        if (teamRequest.getProcessed()) {
            throw new BadRequestException("Team request for user with ID " + userId + " into project with ID " +
                    projectId + " is already processed");
        }
        teamRequest.setProcessed(true);
        transactionTemplate.execute(status -> {
            teamRequestDao.update(teamRequest);
            Team team = teamDao.findById(projectId);
            if (team == null) {
                team = new Team(projectId, null, new String[]{currentUser.getId(), userId}, Ints.checkedCast(Instant.now().getEpochSecond()));
                teamDao.insert(team);
            } else {
                String[] newMembers = Arrays.copyOf(team.getMembers(), team.getMembers().length + 1);
                newMembers[newMembers.length - 1] = userId;
                team.setMembers(newMembers);
                teamDao.update(team);
            }
            return null;
        });
        return ImmutableMap.of("id", projectId);
    }

    @GetMapping("/search")
    public Map<String, List<ProjectDto>> searchProjects(@RequestParam("search") String searchString,
                                                        @RequestParam(value = "lastCreatedTimestamp", required = false) Integer lastCreatedTimestamp,
                                                        @RequestParam(value = "lastId", required = false) String lastId,
                                                        @RequestParam(value = "count", defaultValue = "10") int count) {
        Set<String> projectIds = searchService.searchProjects(searchString);
        List<Project> projects = lastCreatedTimestamp != null && lastId != null ?
                dslContext.selectFrom(Tables.PROJECT)
                        .where(Tables.PROJECT.ID.in(projectIds))
                        .orderBy(Tables.PROJECT.CREATED_TIMESTAMP.desc(), Tables.PROJECT.ID.desc())
                        .seekAfter(lastCreatedTimestamp, lastId).limit(count)
                        .fetchInto(Project.class) :
                dslContext.selectFrom(Tables.PROJECT)
                        .where(Tables.PROJECT.ID.in(projectIds))
                        .orderBy(Tables.PROJECT.CREATED_TIMESTAMP.desc(), Tables.PROJECT.ID.desc())
                        .limit(count).fetchInto(Project.class);
        String[] authorIds = projects.stream().map(Project::getAuthorId).toArray(String[]::new);
        Map<String, User> authors = userDao.fetchById(authorIds)
                .stream().collect(Collectors.toMap(User::getId, Function.identity()));
        return ImmutableMap.of("projects", projects.stream().map(project -> {
            User author = authors.get(project.getAuthorId());
            if (author == null) {
                throw new IllegalStateException("User with ID " + project.getAuthorId() + " has not been found unexpectedly");
            }
            return new ProjectDto(project, author);
        }).collect(Collectors.toList()));
    }
}
