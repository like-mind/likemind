package com.likemind.project.validate;

import com.likemind.project.dto.ProjectDto;

import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AtLeastOneParamPresentValidator implements ConstraintValidator<AtLeastOneParamPresent, ProjectDto> {

    public void initialize(AtLeastOneParamPresent constraint) {
    }

    public boolean isValid(ProjectDto project, ConstraintValidatorContext context) {
        return StringUtils.isNotBlank(project.title)
                || StringUtils.isNotBlank(project.content)
                || project.country!=null
                || project.city!=null
                || project.postalCode !=null
                || project.categories!=null
                || project.skills != null
                || project.resources != null;
    }
}
