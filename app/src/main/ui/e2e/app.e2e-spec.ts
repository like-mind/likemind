import {LikemindPage} from './app.po';

describe('likemind App', function() {
  let page: LikemindPage;

  beforeEach(() => {
    page = new LikemindPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
