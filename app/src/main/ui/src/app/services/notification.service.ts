import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import {SecureRequestService} from './secure-request.service';
import {Observable} from 'rxjs';
import {IdResponse, Notification, NotificationsResponse} from '../dto';
import {map} from 'rxjs/operators';


@Injectable()
export class NotificationsService {
  constructor(private securerequestservice: SecureRequestService) {
  }

  getNotifications(): Observable<Notification[]> {
    return this.securerequestservice.secureGet<NotificationsResponse>('api/notifications')
      .pipe(
        map(response => response.notifications)
      );
  }

  processNotification(id: string): Observable<IdResponse> {
    return this.securerequestservice.securePost<undefined, IdResponse>('api/notifications/' + id);
  }
}
