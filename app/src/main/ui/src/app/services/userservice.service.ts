import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {SecureRequestService} from './secure-request.service';
import {IdResponse, ProfileLogoResponse, User, UserCreateRequest} from '../dto';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class UserService {

  constructor(private http: HttpClient, private router: Router,
              private securerequestservice: SecureRequestService) {
  }

  getCurrentUser(): Observable<User> {
    return this.http.post<User>('/api/users/current',
      null, {
        headers: new HttpHeaders({'X-Requested-With': 'XMLHttpRequest'}),
        withCredentials: true
      })
      .pipe(
        map(user => {
          user.authenticated = true;
          return user;
        }),
        catchError((err: HttpErrorResponse) => {
          if (err.status === 401) {
            return of(new User());
          }
          return throwError(err);
        })
      );
  }

  getCurrentUserSecure(): Observable<User> {
    return this.securerequestservice.securePost<undefined, User>('/api/users/current');
  }

  loginUser(email: string, password: string): Observable<User> {
    return this.http.post<User>('/api/users/current', null, {
      headers: new HttpHeaders({
        'Authorization': 'Basic ' + btoa(email + ':' + password),
        'X-Requested-With': 'XMLHttpRequest'
      }),
      withCredentials: true
    });
  }

  logoutUser(): Observable<string> {
    return this.http.post('/api/logout', null, {
      headers: new HttpHeaders({'X-Requested-With': 'XMLHttpRequest'}),
      withCredentials: true, responseType: 'text' as 'text'
    });
  }

  registerUser(userRequest: UserCreateRequest): Observable<IdResponse> {
    return this.securerequestservice.securePost<UserCreateRequest, IdResponse>('/api/users', userRequest);
  }

  saveProfileImage(formData: FormData): Observable<string> {
    return this.securerequestservice.securePut<FormData, ProfileLogoResponse>('/api/users/profile/logo', formData)
      .pipe(
        map(response => response.url)
      );
  }
}
