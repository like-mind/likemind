import {Injectable} from '@angular/core';
import {Socket} from 'ngx-socket-io';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';
import {ChatMessage, Notification} from '../dto';

@Injectable()
export class ChatSocket extends Socket {

  constructor() {
    super({url: environment.chatServerURI + '/chat'});
  }

  onChatMessage(): Observable<ChatMessage> {
    return this.fromEvent('chat_message');
  }

  onChatError(): Observable<string> {
    return this.fromEvent('chat_error');
  }

  onReconnect(): Observable<void> {
    return this.fromEvent('reconnect');
  }

  onError(): Observable<string> {
    return this.fromEvent('error');
  }

  onAuthError(): Observable<string> {
    return this.fromEvent('not_authenticated');
  }

  sendChatInfo(info: ChatInfo): void {
    this.emit('chat_info', info);
  }
}

export interface ChatInfo {
  chat_id: string;
  is_team: boolean;
}

@Injectable()
export class NotificationSocket extends Socket {

  constructor() {
    super({url: environment.chatServerURI + '/notifications'});
  }

  onNotification(): Observable<Notification> {
    return this.fromEvent('notification');
  }

  onError(): Observable<string> {
    return this.fromEvent('error');
  }
}
