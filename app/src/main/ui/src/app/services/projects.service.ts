import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import {SecureRequestService} from './secure-request.service';
import {Observable} from 'rxjs';
import {ApplyTeamRequest, IdResponse, Project, ProjectCreateRequest, ProjectsResponse} from '../dto';
import {map} from 'rxjs/operators';


@Injectable()
export class ProjectsService {
  constructor(private securerequestservice: SecureRequestService) {
  }

  getProjects(): Observable<Project[]> {
    return this.securerequestservice.secureGet<ProjectsResponse>('/api/projects')
      .pipe(
        map(response => response.projects)
      );
  }

  getMoreProjects(page: number): Observable<Project[]> {
    return this.securerequestservice.secureGet<ProjectsResponse>('/api/projects?pageNumber=' + page)
      .pipe(
        map(response => response.projects)
      );
  }

  getProjectById(id: string): Observable<Project> {
    return this.securerequestservice.secureGet<Project>('/api/projects/' + id);
  }

  getIntroProjects(): Observable<Project[]> {
    return this.securerequestservice.secureGet<ProjectsResponse>('/api/projects/intro')
      .pipe(
        map(response => response.projects)
      );
  }

  createProject(title: string, content: string): Observable<IdResponse> {
    return this.securerequestservice.securePost<ProjectCreateRequest, IdResponse>('/api/projects', {
      title,
      content
    });
  }

  deleteProject(id: string): Observable<IdResponse> {
    return this.securerequestservice.secureDelete<IdResponse>('/api/projects/' + id);
  }

  projectTeamApprove(projectId: string, userId: string): Observable<IdResponse> {
    return this.securerequestservice.securePut<undefined, IdResponse>('/api/projects/' + projectId + '/teams/users/' + userId);
  }

  projectTeamRequest(id: string, coverLetter: string): Observable<IdResponse> {
    return this.securerequestservice.securePost<ApplyTeamRequest, IdResponse>('/api/projects/' + id + '/teams', {
      coverLetter
    });
  }

  addProjectLike(projectId: string): Observable<IdResponse> {
    return this.securerequestservice.securePut<undefined, IdResponse>('/api/projects/' + projectId + '/likes/');
  }

  deleteProjectLike(projectId: string): Observable<IdResponse> {
    return this.securerequestservice.secureDelete<IdResponse>('/api/projects/' + projectId + '/likes/');
  }

  projectSearch(searchString: string): Observable<Project[]> {
    return this.securerequestservice.secureGet<ProjectsResponse>('/api/projects/search?search=' + searchString)
      .pipe(
        map(response => response.projects)
      );
  }
}
