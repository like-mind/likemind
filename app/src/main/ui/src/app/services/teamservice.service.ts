import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {SecureRequestService} from './secure-request.service';
import {Observable} from 'rxjs';
import {Team} from '../dto';

@Injectable()
export class TeamService {

  constructor(private router: Router,
              private securerequestservice: SecureRequestService) {
  }

  getTeamById(id: string): Observable<Team> {
    return this.securerequestservice.secureGet<Team>('api/teams/' + id);
  }
}
