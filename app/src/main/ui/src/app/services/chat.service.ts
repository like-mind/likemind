import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Rx';
import {SecureRequestService} from './secure-request.service';
import {Observable} from 'rxjs';
import {ChatMessage, ChatMessagesResponse, IdResponse} from '../dto';
import {map} from 'rxjs/operators';


@Injectable()
export class ChatService {
  constructor(private securerequestservice: SecureRequestService) {
  }

  initPrivateChat(userId: string): Observable<IdResponse> {
    return this.securerequestservice.securePut<undefined, IdResponse>('api/chat/private/' + userId + '/init');
  }

  getMessages(id: string, reset: boolean): Observable<ChatMessage[]> {
    return this.securerequestservice.secureGet<ChatMessagesResponse>('/api/chat/' + id + '?reset=' + reset)
      .pipe(
        map(res => res.messages)
      );
  }
}
