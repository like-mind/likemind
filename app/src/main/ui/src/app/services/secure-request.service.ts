import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';


@Injectable()
export class SecureRequestService {

  constructor(private http: HttpClient, private router: Router) {
  }

  secureHttp<Request, Response>(method: string, url: string, body: Request, formData = false): Observable<Response> {
    let headers = new HttpHeaders({'X-Requested-With': 'XMLHttpRequest'});
    if (!formData) {
      headers = headers.set('Content-Type', 'application/json');
    }
    const options = {
      method: method,
      headers: headers,
      withCredentials: true,
      body: body
    };
    return this.http.request<Response>(method, url, options)
      .pipe(
        catchError((err: HttpErrorResponse) => {
          if (err.status === 401) {
            this.router.navigate(['/signin']);
          } else {
            return throwError(err);
          }
        })
      );
  }

  secureGet<Response>(url: string): Observable<Response> {
    return this.secureHttp<any, Response>('get', url, null);
  }

  securePut<Request, Response>(url: string, body?: Request): Observable<Response> {
    if (body instanceof FormData) {
      return this.secureHttp<FormData, Response>('put', url, body, true);
    }
    return this.secureHttp<Request, Response>('put', url, body, false);
  }

  securePost<Request, Response>(url: string, body?: Request): Observable<Response> {
    if (body instanceof FormData) {
      return this.secureHttp<FormData, Response>('put', url, body, true);
    }
    return this.secureHttp<Request, Response>('post', url, body, false);
  }

  secureDelete<Response>(url: string): Observable<Response> {
    return this.secureHttp<any, Response>('delete', url, null, false);
  }
}
