import {Component, HostListener, OnInit} from '@angular/core';
import {UserService} from '../../services/userservice.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import {BaseComponent} from '../base.component';
import {throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent extends BaseComponent implements OnInit {

  constructor(private router: Router, private userService: UserService) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(this.userService.getCurrentUser()
      .pipe(
        map(user => {
          if (user.authenticated) {
            this.router.navigate(['/projects']);
          }
        }),
        catchError((err: HttpErrorResponse) => {
          console.log('Something went wrong!!');
          return throwError(err);
        })
      )
      .subscribe());
  }

  @HostListener('window:scroll', ['$event'])
  onScrollEvent($event: Event): void {
    let scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    let element = document.getElementsByClassName('services');
    if (scrollTop > 200) {
      for (let k of Object.keys(element)) {
        element[k].classList.add('active');
      }
    } else {
      for (let k of Object.keys(element)) {
        element[k].classList.remove('active');
      }
    }
  }

}
