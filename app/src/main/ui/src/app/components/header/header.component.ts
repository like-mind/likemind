import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/userservice.service';
import {Router} from '@angular/router';
import {NotificationsService} from '../../services/notification.service';
import {HttpErrorResponse} from '@angular/common/http';
import {BaseComponent} from '../base.component';
import {Observable, of, throwError} from 'rxjs';
import {Notification, PrivateChatMessageContent, TeamChatMessageContent, TeamRequestContent, User} from '../../dto';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {NotificationSocket} from '../../services/socket.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent extends BaseComponent implements OnInit {
  currentUser: User = new User();
  notifications: Notification[] = null;
  noNewNotifications = false;

  constructor(private router: Router, private userService: UserService, private notificationsService: NotificationsService,
              private socket: NotificationSocket) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(this.userService.getCurrentUser()
      .pipe(
        mergeMap(user => {
          this.currentUser = user;
          if (user.authenticated) {
            return this.getNotificationsFirstPage()
              .pipe(
                map(() => this.initNotificationsSocket())
              );
          }
          return of();
        }),
        catchError((err: HttpErrorResponse) => {
          console.log('Something went wrong!!');
          return throwError(err);
        })
      )
      .subscribe());
  }

  getNotificationsFirstPage(): Observable<void> {
    return this.notificationsService.getNotifications()
      .pipe(
        map(notifications => {
          if (notifications && notifications.length > 0) {
            this.notifications = notifications;
          } else {
            this.noNewNotifications = true;
          }
        })
      );
  }

  initNotificationsSocket(): void {
    this.subscriptions.add(this.socket.onNotification()
      .pipe(
        map(notification => {
          console.log(notification);
          if (notification.id && notification.content) {
            this.notifications.unshift(notification);
          }
        })
      )
      .subscribe());
    this.subscriptions.add(this.socket.onError()
      .pipe(
        map(error => {
          console.log(error);
          if (error === 'not_authenticated') {
            this.router.navigate(['/signin']);
          }
        })
      )
      .subscribe());
  }

  logout(): void {
    this.subscriptions.add(this.userService.logoutUser()
      .pipe(
        map(() => {
          this.router.navigate(['/']);
        }),
        catchError((err: HttpErrorResponse) => {
          console.log('Some bad things happen');
          return throwError(err);
        })
      )
      .subscribe());
  }

  handleNotification(notification: Notification): void {
    let route;
    switch (notification.content.type) {
      case 'privateChatMessage':
        route = () => this.router.navigate(['chats', (notification.content as PrivateChatMessageContent).chatId]);
        break;
      case 'teamChatMessage':
        route = () => this.router.navigate(['teams', (notification.content as TeamChatMessageContent).teamId]);
        break;
      case 'teamRequest':
        route = () => this.router.navigate(['projects', (notification.content as TeamRequestContent).projectId]);
        break;
      default:
        return;
    }
    this.subscriptions.add(this.notificationsService.processNotification(notification.id)
      .pipe(
        map(route)
      )
      .subscribe());
  }
}
