import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserService} from '../../services/userservice.service';
import {HttpErrorResponse} from '@angular/common/http';
import {CookieService} from 'ngx-cookie';
import {BaseComponent} from '../base.component';
import {throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent extends BaseComponent implements OnInit {
  private loginForm: FormGroup;
  private hiddenMessage = false;

  constructor(private router: Router, private userService: UserService,
              private cookieService: CookieService) {
    super();
    const fb = new FormBuilder();
    this.loginForm = fb.group({
      'email': [null],
      'password': [null]
    });
  }

  ngOnInit(): void {
    this.subscriptions.add(this.userService.getCurrentUser()
      .pipe(
        map(user => {
          if (user.authenticated) {
            this.router.navigate(['/projects']);
            console.log('I am working from login page');
          }
        })
      )
      .subscribe());
  }

  onSubmit(): void {
    this.subscriptions.add(this.userService.loginUser(this.loginForm.value.email, this.loginForm.value.password)
      .pipe(
        map(() => {
          this.router.navigate(['/projects']);
        }),
        catchError((err: HttpErrorResponse) => {
          if (err.status === 401) {
            this.hiddenMessage = true;
          } else {
            console.log('Something went wrong!!');
            return throwError(err);
          }
        })
      )
      .subscribe());
  }

  getCsrfToken(): string {
    return this.cookieService.get('XSRF-TOKEN');
  }
}
