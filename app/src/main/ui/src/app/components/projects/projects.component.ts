import {Component, OnInit} from '@angular/core';
import {ProjectsService} from '../../services/projects.service';
import {UserService} from '../../services/userservice.service';
import {HttpErrorResponse} from '@angular/common/http';
import {BaseComponent} from '../base.component';
import {throwError} from 'rxjs';
import {Project, User} from '../../dto';
import {catchError, map, mergeMap} from 'rxjs/operators';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.css']
})
export class ProjectsComponent extends BaseComponent implements OnInit {
  projectInfos: {
    project: Project;
    likeInfo: {
      isLiked: boolean;
      likesCount: number;
    }
  }[] = [];
  currentPage = 1;
  totalPages: number;
  isShowLoadMore = true;
  currentUser: User;

  constructor(private projectsService: ProjectsService, private userService: UserService) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(this.userService.getCurrentUserSecure()
      .pipe(
        mergeMap(user => {
          this.currentUser = user;
          return this.projectsService.getProjects()
            .pipe(
              map(projects => {
                this.projectInfos = projects.map(project => this.projectToInfo(project));
              })
            );
        }),
        catchError(error => {
          console.log(error);
          return throwError(error);
        })
      )
      .subscribe());
  }

  projectToInfo(project: Project) {
    return {
      project,
      likeInfo: {
        isLiked: project.likedUsers && project.likedUsers.includes(this.currentUser.id),
        likesCount: project.likedUsers && project.likedUsers.length
      }
    };
  }

  switchLike(projectId: string, likeInfo: {
    isLiked: boolean;
    likesCount: number;
  }): void {
    this.subscriptions.add(
      likeInfo.isLiked ?
        this.projectsService
          .deleteProjectLike(projectId)
          .pipe(
            map(data => {
              likeInfo.likesCount--;
              likeInfo.isLiked = false;
              console.log('- 1 likes');
            }),
            catchError((err: HttpErrorResponse) => {
              console.log('Some bad things happen');
              return throwError(err);
            })
          )
          .subscribe() :
        this.projectsService
          .addProjectLike(projectId)
          .pipe(
            map(data => {
              likeInfo.likesCount++;
              likeInfo.isLiked = true;
              console.log('+ 1 likes');
            }),
            catchError((err: HttpErrorResponse) => {
              console.log('Some bad things happen');
              return throwError(err);
            })
          )
          .subscribe()
    );
  }

  runSearch(searchValue: string): void {
    this.subscriptions.add(
      searchValue ?
        this.projectsService.projectSearch(searchValue)
          .pipe(
            map(projects => {
              this.projectInfos = projects.map(project => this.projectToInfo(project));
            })
          )
          .subscribe() :
        this.projectsService.getProjects()
          .pipe(
            map(projects => {
              this.projectInfos = projects.map(project => this.projectToInfo(project));
            })
          )
          .subscribe()
    );
  }

  loadMore(): void {
    if (this.currentPage < this.totalPages) {
      ++this.currentPage;
      this.subscriptions.add(this.projectsService.getMoreProjects(this.currentPage)
        .pipe(
          map(projects => {
            this.projectInfos = this.projectInfos.concat(projects.map(project => this.projectToInfo(project)));
          }),
          catchError(error => {
            console.log(error);
            return throwError(error);
          })
        )
        .subscribe());
    }
    this.isShowLoadMore = this.currentPage < this.totalPages;
  }
}
