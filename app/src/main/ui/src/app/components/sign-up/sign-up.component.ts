import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UserService} from '../../services/userservice.service';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {BaseComponent} from '../base.component';
import {throwError} from 'rxjs';
import {IdResponse, UserCreateRequest} from '../../dto';
import {catchError, map} from 'rxjs/operators';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent extends BaseComponent implements OnInit {
  model: UserCreateRequest = new UserCreateRequest();

  constructor(private http: HttpClient, private router: Router,
              private userService: UserService) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(this.userService.getCurrentUser()
      .pipe(
        map(user => {
          if (user.authenticated) {
            this.router.navigate(['/projects']);
            console.log('I am working from login page');
          }
        }),
        catchError((err: HttpErrorResponse) => {
          console.log('Something went wrong!!');
          return throwError(err);
        })
      )
      .subscribe());
  }

  register(): void {
    this.subscriptions.add(this.userService.registerUser(this.model)
      .pipe(
        map((data: IdResponse) => {
          console.log('Registration successful');
          this.router.navigate(['/signin']);
        }),
        catchError((err: HttpErrorResponse) => {
          console.log('WTF MAN olololo');
          return throwError(err);
        })
      )
      .subscribe());
  }
}
