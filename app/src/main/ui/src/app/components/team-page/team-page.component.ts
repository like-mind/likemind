import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {TeamService} from '../../services/teamservice.service';
import {UserService} from '../../services/userservice.service';
import {ChatService} from '../../services/chat.service';
import {BaseComponent} from '../base.component';
import {Observable, throwError} from 'rxjs';
import {BriefUser, ChatMessage} from '../../dto';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {ChatSocket} from '../../services/socket.service';

@Component({
  selector: 'app-team-page',
  templateUrl: './team-page.component.html',
  styleUrls: ['./team-page.component.css']
})
export class TeamPageComponent extends BaseComponent implements OnInit {
  private messages: Array<ChatMessage> = [];
  private teamId: string;
  private teamMembers: BriefUser[];
  private messageForm: FormGroup;

  constructor(route: ActivatedRoute, private teamService: TeamService,
              private userService: UserService, private router: Router,
              private chatService: ChatService, private socket: ChatSocket) {
    super();
    this.teamId = route.snapshot.params['teamId'];
    const mf = new FormBuilder();
    this.messageForm = mf.group({
      'messages': [null]
    });
  }

  ngOnInit(): void {
    this.subscriptions.add(this.teamService.getTeamById(this.teamId)
      .pipe(
        mergeMap(team => {
          console.log(JSON.stringify(team));
          this.teamMembers = team.members;
          return this.getMessagesFirstPage();
        }),
        map(() => {
          this.initChatSocket();
        }),
        catchError(err => {
          console.log('Error happened: ' + err);
          return throwError(err);
        })
      )
      .subscribe());
  }

  initChatSocket(): void {
    let element = document.getElementById('messages');
    this.subscriptions.add(this.socket.onChatMessage()
      .pipe(
        map(response => {
          console.log(response.message);
          if (response.message == null || response.message == '') {
            return false;
          } else {
            let node = document.createElement('li');
            let textnode = document.createTextNode(response.message);
            node.appendChild(textnode);
            element.appendChild(node);
          }
        })
      )
      .subscribe());
    this.subscriptions.add(this.socket.onChatError()
      .pipe(
        map(error => console.log('Error received from the socket: ' + error))
      )
      .subscribe());
    this.subscriptions.add(this.socket.onError()
      .pipe(
        map(error => {
          console.log(error);
          if (error === 'not_authenticated') {
            this.router.navigate(['/signin']);
          }
        })
      )
      .subscribe());
    this.subscriptions.add(this.socket.onAuthError()
      .pipe(
        map(error => {
          console.log(error);
          this.router.navigate(['/signin']);
        })
      )
      .subscribe());
    this.subscriptions.add(this.socket.onReconnect()
      .pipe(
        map(() => {
          this.socket.sendChatInfo({
            chat_id: this.teamId,
            is_team: true
          });
        })
      )
      .subscribe());
    this.socket.sendChatInfo({
      chat_id: this.teamId,
      is_team: true
    });
  }

  onSubmit(): boolean {
    let msg = this.messageForm.value.messages;
    if (msg == null || msg == '') {
      return false;
    } else {
      this.socket.emit('message', msg);
      this.messageForm.reset();
    }
  }

  initPrivateChat(user: BriefUser): void {
    this.subscriptions.add(this.chatService.initPrivateChat(user.id)
      .pipe(
        map(response => this.router.navigate(['chats', response.id]))
      )
      .subscribe());
  }

  getMessagesFirstPage(): Observable<void> {
    return this.chatService.getMessages(this.teamId, true)
      .pipe(
        map((messages) => {
          this.messages = messages;
        })
      );
  }
}
