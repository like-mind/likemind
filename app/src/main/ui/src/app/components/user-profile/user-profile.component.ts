import {Component, OnInit, ViewChild} from '@angular/core';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser';
import {Router} from '@angular/router';
import {UserService} from '../../services/userservice.service';
import {ProjectsService} from '../../services/projects.service';
import {CropperSettings, ImageCropperComponent} from 'ng2-img-cropper';
import {Popup} from 'ng2-opd-popup';
import {BaseComponent} from '../base.component';
import {throwError} from 'rxjs';
import {imgSrcToBlob} from 'blob-util';
import {User} from '../../dto';
import {catchError, map} from 'rxjs/operators';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent extends BaseComponent implements OnInit {
  currentUser: User = new User();
  resultData: { image?: string } = {};

  cropperSettings: CropperSettings;
  @ViewChild('cropper', {static: false}) cropper: ImageCropperComponent;

  constructor(private router: Router,
              private userService: UserService, private projectsService: ProjectsService,
              private sanitizer: DomSanitizer, private popup: Popup) {
    super();
    this.cropperSettings = new CropperSettings();
    this.cropperSettings.width = 200;
    this.cropperSettings.height = 200;
    this.cropperSettings.keepAspect = false;
    this.cropperSettings.croppedWidth = 230;
    this.cropperSettings.croppedHeight = 230;
    this.cropperSettings.canvasWidth = 500;
    this.cropperSettings.canvasHeight = 300;
    this.cropperSettings.minWidth = 100;
    this.cropperSettings.minHeight = 100;
    this.cropperSettings.rounded = true;
    this.cropperSettings.minWithRelativeToResolution = false;
    this.cropperSettings.cropperDrawSettings.strokeColor = 'rgba(255,255,255,1)';
    this.cropperSettings.cropperDrawSettings.strokeWidth = 2;
    this.cropperSettings.noFileInput = true;
    this.cropperSettings.fileType = 'image/png';
  }

  ngOnInit(): void {
    this.subscriptions.add(this.userService.getCurrentUserSecure()
      .pipe(
        map(user => {
          this.currentUser = user;
        }),
        catchError(error => {
          console.log(error);
          return throwError(error);
        })
      )
      .subscribe());
  }

  getProfileImageUrl(): SafeUrl {
    if (this.currentUser.profileLogoUrl) {
      return this.sanitizer.bypassSecurityTrustUrl(this.currentUser.profileLogoUrl);
    }
    return this.sanitizer.bypassSecurityTrustUrl('/assets/user-image.jpg');
  }

  fileChangeListener($event: Event): void {
    let image = new Image();
    let file = ($event.target as HTMLInputElement).files[0];
    let myReader = new FileReader();
    myReader.onloadend = loadEvent => {
      image.src = (loadEvent.target as FileReader).result as string;
      this.cropper.setImage(image);
    };
    myReader.readAsDataURL(file);
  }

  cropImage(): void {
    imgSrcToBlob(this.resultData.image).then((blob: Blob) => {
        const formData = new FormData();
        formData.append('image', blob);
        console.log(formData);
        this.subscriptions.add(this.userService.saveProfileImage(formData)
          .pipe(
            map(profileImageUrl => {
              this.currentUser.profileLogoUrl = profileImageUrl;
              this.popup.hide();
            })
          )
          .subscribe());
      }
    )
      .catch(() => {
        console.log('VSE HUINA');
      });
  }

  openPopUp(): void {
    this.popup.options = {
      header: 'Choose and cropp your image',
      confirmBtnContent: 'Save',
      widthProsentage: 40
    };
    this.popup.show(this.popup.options);
  }
}
