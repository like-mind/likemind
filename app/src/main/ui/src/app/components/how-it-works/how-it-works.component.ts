import {Component, OnInit} from '@angular/core';
import {ProjectsService} from '../../services/projects.service';
import {BaseComponent} from '../base.component';
import {Project} from '../../dto';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.component.html',
  styleUrls: ['./how-it-works.component.css']
})
export class HowItWorksComponent extends BaseComponent implements OnInit {
  projects: Project[] = [];

  constructor(private projectsService: ProjectsService) {
    super();
  }

  ngOnInit(): void {
    this.subscriptions.add(this.projectsService.getIntroProjects()
      .pipe(
        map(projects => {
          this.projects = projects;
        })
      )
      .subscribe());
  }
}
