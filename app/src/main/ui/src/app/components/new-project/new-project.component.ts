import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {ProjectsService} from '../../services/projects.service';
import {BaseComponent} from '../base.component';
import {throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.css']
})
export class NewProjectComponent extends BaseComponent implements OnInit {
  private projectForm: FormGroup;

  constructor(private router: Router, private projectsService: ProjectsService) {
    super();
    const projectF = new FormBuilder();
    this.projectForm = projectF.group({
      'title': [null],
      'content': [null]
    });
  }

  ngOnInit(): void {
  }

  createProject() {
    this.subscriptions.add(this.projectsService.createProject(this.projectForm.value.title, this.projectForm.value.content)
      .pipe(
        map(() => this.router.navigate(['/projects'])),
        catchError(err => {
          console.log(err);
          return throwError(err);
        })
      )
      .subscribe());
  }
}
