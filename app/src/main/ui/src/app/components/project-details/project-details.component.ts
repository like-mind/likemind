import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProjectsService} from '../../services/projects.service';
import {UserService} from '../../services/userservice.service';
import {Popup} from 'ng2-opd-popup';
import {throwError} from 'rxjs';
import {BaseComponent} from '../base.component';
import {Project} from '../../dto';
import {catchError, map, mergeMap} from 'rxjs/operators';

@Component({
  selector: 'app-project-details',
  templateUrl: './project-details.component.html',
  styleUrls: ['./project-details.component.css']
})
export class ProjectDetailsComponent extends BaseComponent implements OnInit {
  project: Project;
  teamRequestSent = false;
  projectId: string;
  coverletter: string;
  isAuthor = false;

  constructor(route: ActivatedRoute,
              private projectsService: ProjectsService,
              private userService: UserService,
              private popup: Popup, private router: Router) {
    super();
    this.projectId = route.snapshot.params['projectId'];
  }

  ngOnInit(): void {
    this.subscriptions.add(this.userService.getCurrentUserSecure()
      .pipe(
        mergeMap(user => {
          return this.projectsService.getProjectById(this.projectId)
            .pipe(
              map(project => {
                if (user.id === project.author.id) {
                  this.isAuthor = true;
                } else {
                  if (user.appliedProjects && user.appliedProjects.includes(project.id)) {
                    this.teamRequestSent = true;
                  }
                }
                this.project = project;
              })
            );
        }),
        catchError(error => {
          console.log(error);
          return throwError(error);
        })
      )
      .subscribe());
  }

  applyProject(): void {
    this.subscriptions.add(this.projectsService.projectTeamRequest(this.project.id, this.coverletter)
      .pipe(
        map(() => {
          this.teamRequestSent = true;
          console.log(this.teamRequestSent);
        })
      )
      .subscribe());
    this.popup.hide();
  }

  approveRequest(userId: string): void {
    this.subscriptions.add(this.projectsService.projectTeamApprove(this.project.id, userId)
      .pipe(
        map((data) => {
          this.project.teamId = data.id;
        })
      )
      .subscribe());
  }

  openPopUp(): void {
    this.popup.options = {
      header: 'Sent short message to project author',
      confirmBtnContent: 'Sent',
      widthProsentage: 40
    };
    this.popup.show(this.popup.options);
  }

  deleteProject(): void {
    this.subscriptions.add(this.projectsService.deleteProject(this.project.id)
      .pipe(
        map(() => {
          console.log('project deleted');
          this.router.navigate(['/projects']);
        })
      )
      .subscribe());
  }
}
