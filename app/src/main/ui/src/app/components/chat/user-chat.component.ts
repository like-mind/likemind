import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ChatService} from '../../services/chat.service';
import {BaseComponent} from '../base.component';
import {ChatMessage} from '../../dto';
import {map} from 'rxjs/operators';
import {ChatSocket} from '../../services/socket.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-user-chat-page',
  templateUrl: './user-chat.component.html',
  styleUrls: ['./user-chat.component.css']
})
export class UserChatComponent extends BaseComponent implements OnInit {
  private chatId: string;
  private messageForm: FormGroup;
  private messages: ChatMessage[] = [];

  constructor(route: ActivatedRoute, private router: Router, private chatService: ChatService,
              private socket: ChatSocket) {
    super();
    this.chatId = route.snapshot.params['chatId'];
    const mf = new FormBuilder();
    this.messageForm = mf.group({
      'messages': [null]
    });
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.getMessageFirstPage()
        .pipe(
          map(() => this.initChatSocket())
        )
        .subscribe());
  }

  initChatSocket(): void {
    let element = document.getElementById('messages');
    this.subscriptions.add(this.socket.onChatMessage()
      .pipe(
        map(response => {
          console.log(response.message);
          if (response.message == null || response.message == '') {
            return false;
          } else {
            let node = document.createElement('li');
            let textnode = document.createTextNode(response.message);
            node.appendChild(textnode);
            element.appendChild(node);
          }
        })
      )
      .subscribe());
    this.subscriptions.add(this.socket.onChatError()
      .pipe(
        map(error => console.log('Error received from the socket: ' + error))
      )
      .subscribe());
    this.subscriptions.add(this.socket.onError()
      .pipe(
        map(error => {
          console.log(error);
          if (error === 'not_authenticated') {
            this.router.navigate(['/signin']);
          }
        })
      )
      .subscribe());
    this.subscriptions.add(this.socket.onAuthError()
      .pipe(
        map(error => {
          console.log(error);
          this.router.navigate(['/signin']);
        })
      )
      .subscribe());
    this.subscriptions.add(this.socket.onReconnect()
      .pipe(
        map(() => {
          this.socket.sendChatInfo({
            chat_id: this.chatId,
            is_team: false
          });
        })
      )
      .subscribe());
    this.socket.sendChatInfo({
      chat_id: this.chatId,
      is_team: false
    });
  }

  getMessageFirstPage(): Observable<void> {
    return this.chatService.getMessages(this.chatId, true)
      .pipe(
        map((messages) => {
          this.messages = messages;
          console.log(this.messages);
        })
      );
  }

  onSubmit(): boolean {
    let msg = this.messageForm.value.messages;
    if (msg == null || msg == '') {
      return false;
    } else {
      this.socket.emit('message', msg);
      this.messageForm.reset();
    }
  }
}
