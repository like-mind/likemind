import {RouterModule, Routes} from '@angular/router';
import {SignInComponent} from './components/sign-in/sign-in.component';
import {HomeComponent} from './components/home/home.component';
import {ProjectsComponent} from './components/projects/projects.component';
import {HowItWorksComponent} from './components/how-it-works/how-it-works.component';
import {SignUpComponent} from './components/sign-up/sign-up.component';
import {NewProjectComponent} from './components/new-project/new-project.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import {ProjectDetailsComponent} from './components/project-details/project-details.component';
import {TeamPageComponent} from './components/team-page/team-page.component';
import {UserChatComponent} from './components/chat/user-chat.component';


const route: Routes = [
  {path: '', component: HomeComponent},
  {path: 'signin', component: SignInComponent},
  {path: 'projects', component: ProjectsComponent},
  {path: 'how-it-works', component: HowItWorksComponent},
  {path: 'sign-up', component: SignUpComponent},
  {path: 'new-project', component: NewProjectComponent},
  {path: 'my-profile', component: UserProfileComponent},
  {path: 'projects/:projectId', component: ProjectDetailsComponent},
  {path: 'teams/:teamId', component: TeamPageComponent},
  {path: 'chats/:chatId', component: UserChatComponent}
];

export const routing = RouterModule.forRoot(route);
