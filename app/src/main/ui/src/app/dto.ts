export class IdResponse {
  id: string;
}

export type Availability = 'LESS_THAN_TWENTY' | 'FROM_TWENTY_TO_FORTY' | 'MORE_THAN_FORTY';

export class BriefUser {
  id: string;
  firstName: string;
  lastName: string;
}

export class User extends BriefUser {
  email: string;
  skills: string[];
  resources: string[];
  availability?: Availability;
  country?: string;
  city?: string;
  profileLogoUrl?: string;
  appliedProjects?: string[];
  authenticated: boolean = false;
}

export class UserCreateRequest {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  passwordConfirmation: string;
}

export class UserUpdateRequest {
  firstName?: string;
  lastName?: string;
  email?: string;
  password?: string;
  passwordConfirmation?: string;
  skills?: string[];
  resources?: string[];
  availability?: Availability;
  country?: string;
  city?: string;
}

export class ProfileLogoResponse {
  url: string;
}

export class Team {
  projectId: string;
  name?: string;
  members: BriefUser[];
}

export class TeamRequest {
  coverLetter: string;
  appliedUser: BriefUser;
}

export class ApplyTeamRequest {
  coverLetter: string;
}

export class Project {
  id: string;
  title: string;
  content: string;
  author: BriefUser;
  categories: string[];
  skills: string[];
  resources: string[];
  createdTimestamp: number;
  likedUsers: string[];
  teamRequests?: TeamRequest[];
  teamId?: string;
}

export class ProjectCreateRequest {
  title: string;
  content: string;
}

export class ProjectUpdateRequest {
  title?: string;
  content?: string;
  categories?: string[];
  skills?: string[];
  resources?: string[];
  country?: string;
  city?: string;
  postalCode?: string;
}

export class ProjectsResponse {
  projects: Project[];
}

class NotificationContent {
  type: string;
  message: string;
}

export class PrivateChatMessageContent extends NotificationContent {
  type: 'privateChatMessage';
  chatId: string;
}

export class TeamChatMessageContent extends NotificationContent {
  type: 'teamChatMessage';
  teamId: string;
}

export class TeamRequestContent extends NotificationContent {
  type: 'teamRequest';
  projectId: string;
}

export class Notification {
  id: string;
  content: PrivateChatMessageContent | TeamChatMessageContent | TeamRequestContent;
}

export class NotificationsResponse {
  notifications: Notification[];
}

export class ChatMessage {
  id: string;
  message: string;
  sender: User;
  sentTimestamp: number;
}

export class ChatMessagesResponse {
  messages: ChatMessage[];
}
