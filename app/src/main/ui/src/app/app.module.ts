import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {routing} from './app.routes';
import {HomeComponent} from './components/home/home.component';
import {SignInComponent} from './components/sign-in/sign-in.component';
import {ProjectsComponent} from './components/projects/projects.component';
import {HeaderComponent} from './components/header/header.component';
import {HowItWorksComponent} from './components/how-it-works/how-it-works.component';
import {SignUpComponent} from './components/sign-up/sign-up.component';
import {ProjectsService} from './services/projects.service';
import {SecureRequestService} from './services/secure-request.service';
import {UserService} from './services/userservice.service';
import {NewProjectComponent} from './components/new-project/new-project.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import {ProjectDetailsComponent} from './components/project-details/project-details.component';
import {TeamPageComponent} from './components/team-page/team-page.component';
import {PopupModule} from 'ng2-opd-popup';
import {ImageCropperComponent} from 'ng2-img-cropper';
import {UserChatComponent} from './components/chat/user-chat.component';
import {TeamService} from './services/teamservice.service';
import {CookieModule} from 'ngx-cookie';
import {NotificationsService} from './services/notification.service';
import {ChatService} from './services/chat.service';
import {ChatSocket, NotificationSocket} from './services/socket.service';
import {BsDropdownModule} from 'ngx-bootstrap/dropdown';

@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    HomeComponent,
    ProjectsComponent,
    HeaderComponent,
    HowItWorksComponent,
    SignUpComponent,
    NewProjectComponent,
    UserProfileComponent,
    ProjectDetailsComponent,
    TeamPageComponent,
    ImageCropperComponent,
    UserChatComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    routing,
    PopupModule.forRoot(),
    CookieModule.forRoot(),
    BsDropdownModule.forRoot()
  ],
  providers: [ProjectsService, SecureRequestService, UserService, TeamService, NotificationsService, ChatService,
    ChatSocket, NotificationSocket],
  bootstrap: [AppComponent]
})
export class AppModule {
}
