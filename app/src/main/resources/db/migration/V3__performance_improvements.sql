CREATE INDEX chat_message_chat_id_idx ON chat_message (chat_id);

CREATE UNIQUE INDEX chat_message_sent_timestamp_id_desc_idx ON chat_message (sent_timestamp DESC, id DESC);

CREATE UNIQUE INDEX private_chat_user_id_another_id_idx ON private_chat (user_id, another_user_id);

CREATE UNIQUE INDEX project_created_timestamp_id_desc_idx ON project (created_timestamp DESC, id DESC);

CREATE INDEX project_author_id_idx ON project (author_id);

CREATE UNIQUE INDEX user_email_idx ON "user" (email);

CREATE INDEX notification_user_id_idx ON notification (receiver_id);

CREATE INDEX team_request_user_id_idx ON team_request (user_id);
