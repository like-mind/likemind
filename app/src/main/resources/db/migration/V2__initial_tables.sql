CREATE TABLE "user"
(
    id                text PRIMARY KEY,
    email             text,
    first_name        text,
    last_name         text,
    password_hash     text,
    skills            text[],
    resources         text[],
    availability      text,
    country           text,
    city              text,
    role              text,
    profile_logo_url  text,
    created_timestamp integer
);

CREATE TABLE project
(
    id                text PRIMARY KEY,
    title             text,
    content           text,
    author_id         text REFERENCES "user" (id),
    categories        text[],
    skills            text[],
    resources         text[],
    country           text,
    city              text,
    postal_code       text,
    liked_users       text[],
    created_timestamp integer
);

CREATE TABLE team
(
    project_id        text REFERENCES project (id) PRIMARY KEY,
    name              text,
    members           text[],
    created_timestamp integer
);

CREATE TABLE team_request
(
    project_id        text REFERENCES project (id),
    user_id           text REFERENCES "user" (id),
    cover_letter      text,
    created_timestamp integer,
    processed         boolean,
    PRIMARY KEY (project_id, user_id)
);

CREATE TABLE notification
(
    id             text PRIMARY KEY,
    content        json,
    receiver_id    text REFERENCES "user" (id),
    sent_timestamp integer,
    processed      boolean
);

CREATE TABLE social_connection
(
    provider_id      text,
    provider_user_id text,
    app_user_id      text,
    PRIMARY KEY (provider_id, provider_user_id)
);

CREATE TABLE chat_message
(
    id             text PRIMARY KEY,
    message        text,
    sender_id      text,
    sent_timestamp integer,
    chat_id        text
);

CREATE TABLE private_chat
(
    id              text PRIMARY KEY,
    user_id         text REFERENCES "user" (id),
    another_user_id text REFERENCES "user" (id)
);
