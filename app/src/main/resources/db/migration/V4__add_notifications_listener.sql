CREATE OR REPLACE FUNCTION notify_new_notification() RETURNS trigger AS
$BODY$
BEGIN
    PERFORM pg_notify('new_notification', row_to_json(NEW)::text);
    RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql VOLATILE
                        COST 100;

CREATE TRIGGER notify_new_notification
    AFTER INSERT
    ON "notification"
    FOR EACH ROW
EXECUTE PROCEDURE notify_new_notification();