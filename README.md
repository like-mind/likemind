# Likemind
Likemind is a place where you can find your like-mind.

## Development

- Build the project using `mvn clean install`. Note: a PostgreSQL instance needs to be running locally so that JOOQ 
can generate necessary classes. by default the JOOQ Maven plugin will look up it by url `jdbc:postgresql://localhost:5432/likemind`, 
username `postgres` and password `123`. Might be changed accordingly in `app/pom.xml` plugin section.
- Start Redis with `run_redis.sh`. In order for command to work `REDIS_HOME` must be set.
- Start ElasticSearch with `run_elastic.sh`. In order for command to work `ELASTIC_HOME` must be set.
- Start Websocket (Chat and Notifications) server with `cd websocket; npm run start`. Optional `POSTGRESQL_URL` can be 
specified for PostgreSQL database used for notifications.
- Start the API server with `mvn spring-boot:run -Dspring.profiles.active=dev <other config properties if needed>`
 from the directory `app`

#### Configuration properties can be specified inside `application.yml` or as environment variables

## External services

- **Postgres**:
    - configuration properties:
        - `spring.datasource.url` - JDBC url to the PostgreSQL database (default: `jdbc:postgresql://localhost:5432/likemind`).
        - `spring.datasource.username` - Username to the PostgreSQL database (default: `postgres`).
        - `spring.datasource.password` - Password to the PostgreSQL database (default: `123`. **NEVER COMMIT YOUR OWN PASSWORD TO BITBUCKET**).
        
- **Elasticsearch**:
    - configuration properties:
        - `spring.elasticsearch.jest.uris` - URIs of Elasticsearch instances (default: `http://localhost:9200`). 
             
- **Redis**:
    - configuration properties:
        - `spring.redis.host` - Host of the Redis server (default: `localhost`).     
        - `spring.redis.port` - Port of the Redis server (default: `6379`).             

- **AWS S3**:
    - configuration properties:
        - `cloud.aws.region.static` - Region of the S3 buckets (default: `us-east-1`).     
        - `cloud.aws.credentials.accessKey` - Access Key to AWS (required. **NEVER COMMIT TO BITBUCKET**).   
        - `cloud.aws.credentials.secretKey` - Secret Key to AWS (required. **NEVER COMMIT TO BITBUCKET**).   
- **Facebook**:
    - configuration properties:
        - `spring.social.facebook.app-id` - Application ID from Facebook (required).
        - `spring.social.facebook.app-secret` - Application secret from Facebook (required).
        
## Additional properties
- `transfer.s3-bucket` - S3 bucket for images (default: likemind-dev on 'dev' and so on).

## PostgreSQL
### Flyway
DB migrations are located in the folder `app/src/resources/db/migration`. For each change one has to create a new migration file
which has format `VX__***.sql`, where `X` is an incremental version number.

## AWS S3
S3 bucket must have public access by ACL enabled.

## Commit / Pull request rules

Always include a ref to the issue on Bitbucket you would like to close/resolve into the commit message.
Use # to specify the issue.
Example:
```
#4: Simple login logic on back-end

Added simple login logic and PostgreSQL.
```
Also include a Resolve action for your issue into the Merge message when merging your Pull request.
Examples:   
- Close #123  
- Resolve #656
- Fix #786

Also, please try to create an issue on Bitbucket every time you want to create a Pull request.